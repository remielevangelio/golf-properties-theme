<!-- footer -->
<div id="footer">

 <div class="container">
	<div class="row">
    
	<div class="col-xs-3 col-sm-1 col-md-1 col-lg-1">
    	<img src="<?php bloginfo('template_directory');?>/img/fgpn-xs.png" alt="Finest Golf Property Network" title="Finest Golf Property Network" />
    </div>

    
    <div class="col-xs-3 col-sm-1 col-md-1 col-lg-1">
    	<img src="<?php bloginfo('template_directory');?>/img/re-xs.png" alt="Real Estate" title="Real Estate" />
    </div>
    
     <div class="col-xs-4 col-sm-3 col-md-2 col-lg-2">
    	<div><b>Inquiries:</b></div>
		<div><b><?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Contact Number')) ?></b></div>
		<div class="owner-email"><a href="mailto:<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Contact Email')) ?>"><?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Contact Email')) ?></a></div>
    </div>
    
    <div class="col-xs-12 col-sm-3 col-md-2 col-lg-2 social-networks">
    	<div><b>Connect to Us</b></div>
        <a href="<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('FB Link')) ?>" target="_blank" title="Facebook"><img src="<?php bloginfo('template_directory');?>/img/fb.png" alt="Facebook" title="Facebook" /></a>
        <a href="<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Twitter Link')) ?>" target="_blank" title="Twitter"><img src="<?php bloginfo('template_directory');?>/img/twitter.png" alt="Twitter" title="Twitter" /></a>
        <a href="<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Pinterest Link')) ?>" target="_blank" title="Pinterest"><img src="<?php bloginfo('template_directory');?>/img/pinterest.png" alt="Pinterest" title="Pinterest" /></a>
    </div>
    
     <div class="col-xs-12 col-sm-4 col-md-5 col-lg-5">
    	<?php wp_nav_menu( array( 'theme_location' => 'footer-menu')); ?>
        
        <div class="clearthis"></div>
        <br />
	
	<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Copyright Info')) ?>
    <a href="#" class="register-link">Reg here</a>
    </div>
    
    	</div>
	</div>
</div>
<!-- end footer -->
            
        </div>
	</div>
  </div>
</div>
<!-- end Main -->

<script>
jQuery(document).ready(function($){
	
	$('a[href=#the-front-nine]').click(function(){
        $($(this).attr('href')).modal('show');
    });
    
	
	$("#search-results-box").mCustomScrollbar({
		autoHideScrollbar:true,
		theme:"dark",
		updateOnBrowserResize:true
	});
	
	
	$(".scrollable-content").mCustomScrollbar({
		horizontalScroll:true,
		theme:"dark",
		advanced:{
			autoExpandHorizontalScroll:true,
			updateOnContentResize: true
		}
	});


	//custom select
	    $('.selectbox').customSelect()
		
	/* cufon */
	$(function(){
    Cufon.replace('.main-heading');
	});

	
	/* mobile nav, pls change ID to menu's id */
	$("#menu-main-menu").tinyNav
		({
			 active: 'selected', // String: Set the "active" class
			 header: 'Menu:', // String: Specify text for "header" and show header instead of the active item
			 label: '' // String: Sets the <label> text for the <select> (if not set, no label will be added)
		});
		
		//easing
		$('.tagline a').click(function(){
			$('html, body').animate({
				scrollTop: $( $(this).attr('href') ).offset().top
			}, 500);
			return false;
		});
				
});
</script>
<!-- end scripts -->


<!-- agent modal -->
<div id="myModalagent" class="modal fade">

<div class="modal-dialog">
    <div class="modal-content boxtype">
      <div class="modal-header noborders">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title mypopuptitle" id="myModalLabel">Apply as an Agent</h4>
        <h5 class="modal-sub-title">Please complete the following to registef</h5>
      </div>
      <div class="modal-body">
      	<div id="contact-popup">
      		<?php echo do_shortcode( '[contact-form-7 id="259" title="Apply as Agent Form 2"]' ) ?>
	    </div>
      </div>
    </div>
  </div>
  
</div>
<!-- end agent modal -->
<!-- the-front-nine modal -->
<div id="the-front-nine" class="modal fade">
	<div class="modal-dialog">
	    <div class="modal-content boxtype">
	      <div class="modal-header noborders">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title mypopuptitle" id="myModalLabel">The Front Nine</h4>
	        <h5 class="modal-sub-title">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</h5>
	      </div>
	      <div class="modal-body">
	        <div id="contact-popup">
	            <ul>
	            <?php $properties = property_search('admin');
	            $frontnine = hayme_get_front_nine();
	            foreach($frontnine as $key) { $frontninedata = $properties[0][$key]; ?>
	                <li><a href="/the-front-nine/?listingKey=<?php echo $frontninedata->ListingKey ?>"><?php echo $frontninedata->ListingTitle ?></a></li>
	            <?php
	            } ?>
	            </ul>
	        </div>
	      </div>
	    </div>
  </div>
</div>

<!-- end the-front-nine modal -->
<!-- modal login-->
<div id="myModalLogin" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content boxtype">
			<div class="modal-header noborders">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title mypopuptitle" id="myModalLabel">Scorecard Registration</h4>
				<h5 class="modal-sub-title">Please complete the following to register</h5>
			</div>
			<div class="modal-body">
				<div id="contact-popup">
					<?php login_with_ajax(); ?>

					<div class="clearthis"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end modal login-->

<!-- modal signup-->
<div id="myModalSignup" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content boxtype">
			<div class="modal-header noborders">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title mypopuptitle" id="myModalLabel">Scorecard Registration</h4>
				<h5 class="modal-sub-title">Please complete the following to register</h5>
			</div>
			<div class="modal-body">
				<div id="contact-popup">

					<?php echo do_shortcode( '[ajax_register]' ); ?>
						
					<!-- <form action="">
						<div class="left-col">
							
							<p><label>First Name</label></p>
							<p><label>Last Name</label></p>
							<p><label>Email</label></p>
							<p><label>Password</label></p>
							<p><label>Repeat Password</label></p>
						</div>

						<div class="right-col">
							<p><input type="text" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" name="fname"></p>
							<p><input type="text" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" name="lname"></p>
							<p><input type="email" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" name="email"></p>
							<p><input type="password" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" name="pass1"></p>
							<p><input type="password" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" name="pass2"></p>
							
							<input type="submit" value="Submit →" class="wpcf7-form-control wpcf7-submit s-des btn">

						</div>

					</form> -->
					<div class="clearthis"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- end modal -->
<!-- scorecard modal -->
<?php //include (TEMPLATEPATH . '/includes/my-scorecard.php'); ?>
<?php wpfp_list_favorite_posts(); ?>
<!-- end scorecard modal -->
<?php wp_footer(); ?>


<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
(function($) {
 
/*
*  render_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/
 
function render_map( $el ) {
 
	// var
	var $markers = $el.find('.marker');
 
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
 
	// create map	        	
	var map = new google.maps.Map( $el[0], args);

 
	// add a markers reference
	map.markers = [];
 
	// add markers
	$markers.each(function(){
 
    	add_marker( $(this), map );
 
	});
 
	// center map
	center_map( map );
 
}
 
/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/
 
function add_marker( $marker, map ) {
 
	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
 
	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map
	});
 
	// add to array
	map.markers.push( marker );
 
	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html()
		});
 
		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {
 
			infowindow.open( map, marker );
 
		});
	}
 
}
 
/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/
 
function center_map( map ) {
 
	// vars
	var bounds = new google.maps.LatLngBounds();
 
	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){
 
		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
 
		bounds.extend( latlng );
 
	});
 
	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

	$(document).on('click', '.map', function() {
        google.maps.event.trigger(map, 'resize');
        // only 1 marker?
		if( map.markers.length == 1 )
		{
			// set center of map
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 16 );
		}
		else
		{
			// fit to bounds
			map.fitBounds( bounds );
		}
    });
 
}
 
/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
 
$(document).ready(function(){

 
	$('.acf-map').each(function(){
 
		render_map( $(this) );
 
	});

 
});
 
})(jQuery);
</script>


<!-- For search  -->
<script src='<?php bloginfo('template_directory');?>/js/search.js'></script>
</body>
</html>

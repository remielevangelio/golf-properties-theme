<div id="sidebar-r">
                <?php  if(have_posts()) { while ( have_posts() ) {  the_post(); ?>

				            
            	<div class="item-box">
                <a href="#">
                <img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon1.png" class="sidebar-r-ico" /> 
                </a>
                <div class="sidebar-r-text">
                 <a href="#">Search All Properties
                in this Golf Community
                </a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon2.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#" data-toggle="modal" data-target="#viewCommunityAgentModal">View Agents Specialized in this Golf Community</a>
                </div>
                    
                    <div class="clearthis"></div>
                    <?php community_view_agents_modal(); ?>
                </div>
                
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#" class="get-answer-link" data-toggle="modal" data-target="#getAnswersCommunityModal">Get Answers</a>
                <?php if($_POST['gasubmit']): ?>
                    <script>
                        jQuery(document).ready(function() {
                            jQuery('a.get-answer-link').trigger('click');
                        });
                    </script>
                <?php endif; ?>
                </div>
                
                	<div class="clearthis"></div>
                </div>
            
            
            	 <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon4.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="<?php bloginfo('url'); ?>/community-blog/?id=<?php echo get_the_ID(); ?>">Community Blog</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
            
            
            
            
            <!-- scorecard -->
            <?php if ( is_user_logged_in() ) { ?>

            <div class="scorecard">
            	<div class="scorecard-content">
            		<?php wpfp_link() ?>
                </div>
            </div>
            
            <?php } else { echo "<br/>";} ?>
            
            <!-- more links -->
            
            <div class="more-links">
            	<a href="#" onclick="window.print();return false" ><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
                <a href="mailto:<?php the_field('communityemail')?>"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
                <a href="#" data-toggle="modal" data-target="#shareModal"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
            </div>

            <!-- Share THis  -->
            <?php  share_this_items( get_the_permalink(), get_the_title() ); ?>
        
        
       	</div>
         
         
         	<!-- experts -->
   
         <div id="experts-box">
            <div class="title-box">
            <h2 ><?php the_title(); ?> Experts</h2>
            </div>
        
            <?php
                $email_array = array();

                $agents = get_field('communityagents');
                if($agents) {
                    foreach($agents as $agent) {
                        $agent_id = $agent['ID'];
                        $agent_name = $agent['display_name'];
                        $agent_phone = get_field('agentcontact','user_'.$agent_id);
                        $agent_pic = get_field('profile_picture','user_'.$agent_id);
                        $agent_position = get_field('agentposition','user_'.$agent_id);

                        $agent_email = get_the_author_meta('user_email',$agent_id); 

                        //push email so email array
                        array_push($email_array, $agent_email);

            ?>
                <div class="item-expert">
                    <?php
                        if ( $agent_pic ) {
                    ?>
                        <img src="<?php echo $agent_pic['sizes']['thumbnail']; ?>" class="expert-img" style="width: 74px; height: 74p;" />
                    <?php
                        }else{
                    ?>
                        <img src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" class="expert-img" style="width: 74px; height: 74px;" />
                     <?php
                        }
                    ?>
                    <div class="expert-desc">
                        <h3><?php echo $agent_name; ?></h3>
                        <?php echo $agent_position; ?>
                        <?php echo $agent_phone; ?>
                        <?php //the_field('agentdesignations','user_'.$agent_id);?>
                        <br />
                        <a href="<?php echo site_url();?>/agents/?id=<?php echo $agent_id;?>">View more details »</a>
                    </div>
                    
                    <div class="clearthis"></div>
                
                </div>

            <?php
                    }
                } else {
                    echo "No experts found.";
                }
            ?>
            
            

            <!-- Get answers Modal -->
            <?php get_answers_community_modal($email_array); ?>
        </div>
        
            <?php 
                 } //endwhile
             } // endif

              wp_reset_postdata(); wp_reset_query();?>
        <!-- end experts -->
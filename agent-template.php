<?php
/*

Template Name: Agent Template

*/

?>
<?php get_header(); ?>

<div id="page-content">
    <div class="row">
        <!-- three columns -->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-col">
         
            <div class="search-results">
            
                <div class="sr-headline">
                <?php if(isset($_GET['acountry']) && isset($_GET['astate']) && isset($_GET['acity'])) { ?>
                <h2>Search Results</h2>
                <?php } else { ?>
                <h2>List of Agents</h2>
                <?php } ?>
                <?php
                    if(isset($_GET['acountry']) && isset($_GET['astate']) && isset($_GET['acity'])) {
                            $country_id = (int) mysql_real_escape_string(trim($_GET['acountry']));
                            $state_id = mysql_real_escape_string(trim($_GET['astate']));
                            $city_id = mysql_real_escape_string(trim($_GET['acity']));


                            $args = array(
                                'role' => 'aamrole_53a16309a9826',
                                'meta_query' => array(
                                        array(
                                            'key' => 'agentlocation',
                                            'value' => ':"'.$country_id.'";',
                                            'compare' => 'LIKE'
                                        ),
                                        array(
                                            'key' => 'agentlocation',
                                            'value' => ':"'.$city_id.'";',
                                            'compare' => 'LIKE'
                                        ),
                                        array(
                                            'key' => 'agentlocation',
                                            'value' => ':"'.$state_id.'";',
                                            'compare' => 'LIKE'
                                        ),
                                    ),  
                            );

                    } else {
                            $args = array(
                                'role' => 'aamrole_53a16309a9826',
                            );
                    }


                            $the_query= new WP_User_Query( $args);

                            $matches=0;
                            if( ! empty( $the_query->results )){
                                $matches= $the_query->get_total();
                            }
                ?>
                <div class="sub-head"><?php echo $matches;?> matches</div>
                </div>
                
                
                
                <div id="search-results-box" class="agent-page">
                
                    <!-- <h3>Arizona, USA</h3> -->
                    
                  
                    
                    <br />
                    <?php if(isset($_GET['acountry']) && isset($_GET['astate']) && isset($_GET['acity'])) { ?>
                    <a href="#" class="sr-edit">edit search</a> <a href="#" class="sr-save">save search</a>
                    <?php }  ?>
                        <?php
                                                 
                            $pstc=1;
                           
                              if( ! empty( $the_query->results )){
                               
                                foreach ( $the_query->results as $user ) {
                                    $agent_id = $user->ID;
                                    $agent_email = get_the_author_meta('user_email',$agent_id); 
                                    $profile_pic = get_field('profile_picture','user_'.$agent_id);
                        ?>
                    
                      <!--- search item -->
                    <div class="search-item-b">
                           
                           <?php
                            if ( $profile_pic ) {
                                echo wp_get_attachment_image( $profile_pic['id'], $size = 'thumbnail');
                            }else{
                        ?>
                            <img src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" class="s-property-thumb img-responsive" />
                         <?php
                            }
                        ?>
                        <div class="s-property-desc">
                                <p><div class="s-property-title"><?php echo $user->display_name; ?></div></p>
                                <p><?php the_field('agentcontact','user_'.$agent_id); ?></p>
                                <p><?php the_field('agentbrokerage','user_'.$agent_id);?></p>
                                <p><a href="mailto:<?php echo $agent_email; ?>">Email Agent</a></p>
                                <?php if(($country_id != "") || ($state_id != "") || ($city_id != "")) {  ?>
                                <p><a href="<?php echo site_url();?>/agents/?id=<?php echo $agent_id;?>&acountry=<?php echo $country_id;?>&astate=<?php echo $state_id; ?>&acity=<?php echo $city_id; ?>">View Details »</a></p>
                                <?php } else { ?>
                                <p><a href="<?php echo site_url();?>/agents/?id=<?php echo $agent_id;?>">View Details »</a></p>
                                <?php } ?>
                        </div>
                        <div class="clearthis"></div>
                    </div>
                    
                    <!-- end search item -->
                    <?php
                            }//end foreach
                        } //end if
                    ?>
                </div>
                
            </div>
        </div>
        
            <!-- START MAIN -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">

                    
                    <div id="mid-col-main">
                    <?php

                        if( isset($_GET['id']) && is_numeric($_GET['id']) ) {
                           $agent_id = mysql_real_escape_string(trim($_GET['id']));

                            $aux = get_userdata( $agent_id );

                            if($aux==false) {
                                echo "<h3>User does not exists.</h3>";
                            } else {
                    ?>
                         <div class="featured-image">

                            <div class="profile-box">
                                <div class="apic">
                                    <?php
                                        $profile_pic = get_field('profile_picture','user_'.$agent_id);
                                        $size = 'full';

                                        if ( $profile_pic ) {
                                             echo "<img id='c_agentpicture' src='".$profile_pic['sizes']['thumbnail']."' style='width: 150px;height: 150px;' class='s-property-thumb img-responsive'/>";
                                        } else {
                                    ?>
                                        <img id="c_agentpicture" src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" alt="agent" title="agent" class="agentpic" />
                                    <?php
                                        }
                                    ?>
                                </div>
                                <!-- <img src="<?php bloginfo('template_directory');?>/img/karen-baldwin-profile-pic.jpg" alt="agent" title="agent" class="agentpic" /> -->
                                
                                <div class="profile-description">
                                    <h3 id="c_agentname" class="agent-name"><?php echo get_the_author_meta('display_name',$agent_id); ?></h3>
                                    <div id="c_agentposition" ><?php the_field('agentposition','user_'.$agent_id);?>, <?php the_field('agentdesignations','user_'.$agent_id);?></div>
                                    <div ><?php the_field('agentcontact','user_'.$agent_id);?> | <a href="mailto:<?php echo get_the_author_meta('user_email', $agent_id); ?>">Email Me</a></div>
                                    <br />
                                    <div><?php the_field('agentbrokerage','user_'.$agent_id);?></div>
                                    <div><?php the_field('agentbrokerageaddress','user_'.$agent_id);?></div>
                                    <div id="c_agentbrokeragecity"><?php the_field('agentbrokeragecity','user_'.$agent_id);?>, <span id="c_agentbrokeragestate"><?php the_field('agentbrokeragestate','user_'.$agent_id);?></span> <?php the_field('agentbrokeragezipcode','user_'.$agent_id);?></div>
                                    <br />
                                    <div class="special-exp">Years Experience:   <span class="blackthis"><?php the_field('agentexperience','user_'.$agent_id);?></span></div>
                                    <div class="special-exp">Community Specialist:</div>
                                     <?php

                                        $view_agent = get_the_author_meta('display_name',$agent_id);
                                        $args = array(
                                            'showposts' => -1, 
                                            'post_type' => 'communities',                            
                                            'orderby'          => 'post_date',
                                            'order'            => 'DESC',
                                            'post_status'      => 'publish'
                                        );
                                            
                                            $the_query= new WP_query( $args);
                                            //echo "before if";
                                           //print_r($the_query);
                                            $pstc=1;
                                            //echo count($posts);
                                             if($the_query->have_posts()){
                                                while ( $the_query->have_posts() ) {
                                                    $the_query->the_post();


                                                    $agents = get_field('communityagents');
                                                    if($agents) {
                                                        foreach($agents as $agent) {
                                                            $agent_name = $agent['display_name'];

                                                            if($agent_name == $view_agent) {
                                                ?>
                                                <div>- <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div> 

                                                <?php
                                                            }
                                                        }
                                                    }                                                   
                                                }

                                                wp_reset_postdata();
                                             }


                                         
                                    ?>

                                </div>
                                
                                <div class="clearthis"></div>
                                
                                <div class="biography-box">
                                    <h4 class="bio">Biography</h4>
                                    <?php the_field('agentbio','user_'.$agent_id);?>

                                </div>
                            </div>
                            
                            </div>
                


                    <?php   }
                           
                        } else {
                            if(isset($_GET['acountry']) && isset($_GET['astate']) && isset($_GET['acity'])) {
                                    $country_id = (int) mysql_real_escape_string(trim($_GET['acountry']));
                                    $state_id = mysql_real_escape_string(trim($_GET['astate']));
                                    $city_id = mysql_real_escape_string(trim($_GET['acity']));


                                    $args = array(
                                        'number' => 1,
                                        'role' => 'aamrole_53a16309a9826',
                                        'meta_query' => array(
                                                array(
                                                    'key' => 'agentlocation',
                                                    'value' => ':"'.$country_id.'";',
                                                    'compare' => 'LIKE'
                                                ),
                                                array(
                                                    'key' => 'agentlocation',
                                                    'value' => ':"'.$city_id.'";',
                                                    'compare' => 'LIKE'
                                                ),
                                                array(
                                                    'key' => 'agentlocation',
                                                    'value' => ':"'.$state_id.'";',
                                                    'compare' => 'LIKE'
                                                ),
                                            ),  
                                    );

                            } else {
                                    $args = array(
                                        'number' => 1,
                                        'role' => 'aamrole_53a16309a9826',
                                    );
                            }

                            $agent_query2= new WP_User_Query( $args);

                            if(! empty( $agent_query2->results )) {

                                foreach($agent_query2->results as $user2) {
                                    $agent_email = get_the_author_meta('user_email',$user2->ID); 
                            
                    ?>
            
                            <div class="featured-image">

                                <div class="profile-box">
                                    <div class="apic">
                                        <?php
                                               $profile_pic = get_field('profile_picture','user_'.$user2->ID);
                                        $size = 'full';

                                            if ( $profile_pic ) {
                                                 echo "<img id='c_agentpicture' src='".$profile_pic['sizes']['thumbnail']."' style='width: 150px;height: 150px;' class='s-property-thumb img-responsive'/>";
                                            } else {
                                        ?>
                                            <img id="c_agentpicture" src="<?php bloginfo('template_directory');?>/img/default_avatar.jpg" alt="agent" title="agent" class="agentpic" />
                                        <?php
                                            }
                                        ?>
                                    </div>
                                        
                                    
                                    <div class="profile-description">
                                        <h3 id="c_agentname" class="agent-name"><?php echo $user2->display_name; ?></h3>
                                        <div id="c_agentposition"><?php the_field('agentposition','user_'.$user2->ID);?>, <?php the_field('agentdesignations','user_'.$user2->ID);?></div>
                                        <div id="c_agentcontact"><?php the_field('agentcontact','user_'.$user2->ID);?> | <a href="mailto:<?php echo $agent_email; ?>">Email Me</a></div>
                                        <br />
                                        <div><?php the_field('agentbrokerage','user_'.$user2->ID);?></div>
                                        <div><?php the_field('agentbrokerageaddress','user_'.$user2->ID);?></div>
                                        <div id="c_agentbrokeragecity"><?php the_field('agentbrokeragecity','user_'.$user2->ID);?>,  <span id="c_agentbrokeragestate"><?php the_field('agentbrokeragestate','user_'.$user2->ID);?></span> <?php the_field('agentbrokeragezipcode','user_'.$user2->ID);?></div>
                                        <br />
                                        <div class="special-exp">Years Experience:   <span class="blackthis"><?php the_field('agentexperience','user_'.$user2->ID);?></span></div>
                                        <div class="special-exp">Community Specialist:</div>
                                        <?php 
                                            $view_agent = get_the_author_meta('display_name',$user2->ID);
                                            $args = array(
                                                'showposts' => -1, 
                                                'post_type' => 'communities',                            
                                                'orderby'          => 'post_date',
                                                'order'            => 'DESC',
                                                'post_status'      => 'publish'
                                            );
                                                
                                                $the_query= new WP_query( $args);
                                                //echo "before if";
                                               //print_r($the_query);
                                                $pstc=1;
                                                //echo count($posts);
                                                 if($the_query->have_posts()){
                                                    while ( $the_query->have_posts() ) {
                                                        $the_query->the_post();


                                                        $agents = get_field('communityagents');
                                                        if($agents) {
                                                            foreach($agents as $agent) {
                                                                $agent_name = $agent['display_name'];

                                                                if($agent_name == $view_agent) {
                                                    ?>
                                                    <div>- <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div> 

                                                    <?php
                                                                }
                                                            }
                                                        }                                                   
                                                    }

                                                    wp_reset_postdata();
                                                 }

                                         ?>

                                    </div>
                                    
                                    <div class="clearthis"></div>
                                    
                                    <div class="biography-box">
                                        <h4 class="bio">Biography</h4>
                                        <?php the_field('agentbio','user_'.$user2->ID);?>

                                    </div>
                                </div>
                                
                            </div>
                   
                         <?php
                                }
                            } else {
                                echo "<h3>No user found.</h3>";
                            }//end if 

                        }
                        ?>
                         </div>
            </div>
            <!-- END MAIN -->
        
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
        
           <?php get_sidebar('agents2'); ?>
        
        
        </div>
         
        
         
        </div>
        <!-- end three columns -->
        
     
        <div class="clearthis"></div>
        
        

        </div>
    </div>
</div>

<?php get_footer(); ?>
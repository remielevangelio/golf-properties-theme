<?php
/*

Template Name: Property Search Template

*/
if (isset($_REQUEST['pscountry'])) {
    $_SESSION['property-search'] = serialize($_REQUEST);
}

$order_search_result = (isset($_SESSION['order_search_result'])?$_SESSION['order_search_result']:'desc');
list($listing, $price) = property_search($order_search_result);
$mylisting = array();

if(isset($_GET['listingKey']) && isset($listing[$_GET['listingKey']])) {
    $mylisting = $listing[$_GET['listingKey']];
} else {
    $array_keys = array_keys($price);
    $mylisting = $listing[$array_keys[0]];
}
//print_r($mylisting);
$propertyimg = (string)(is_array($mylisting->Photos->Photo)?$mylisting->Photos->Photo[0]->MediaURL:$mylisting->Photos->Photo->MediaURL);

$agent = get_agent_by_listingkey(((string)$mylisting->ListingKey));

$community = get_community_by_listing_key((string)$mylisting->ListingKey);
$community_id = "#";
$community_name = "Not Set";
$community_link = '#';

if(!empty($community)) {
    $community_id = $community->ID;
    $community_name = $community->post_title;
    $community_link = get_bloginfo('url').'?p='.$community->ID;
} 

if($mylisting) {
    $listing_address = $mylisting->Address->children('commons', true);
}
?>
<?php get_header(); ?>

<div id="page-content">
	<div class="row">
    	<!-- three columns -->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-col">
         
        	<div class="search-results">
            
            	<div class="sr-headline">
                <?php if(isset($_GET['comproperty-search'])): ?>
                <h2>Community List Search Results</h2>
                <?php elseif(isset($_GET['showupdatedproperty'])): ?>
                <h2>Updated Property Listings</h2>
                <?php elseif(isset($_GET['shownewproperty'])): ?>
                <h2>New Property Listings</h2>
                <?php elseif(isset($_GET['viewlistings'])): ?>
                <h2>Agent List Search Results</h2>
                <?php else: ?>
            	<h2>Search Results</h2>
                <?php endif; ?>
                <div class="sub-head"><?php echo number_format(count($price)) ?> matches</div>
                </div>
                
                
                
                <div id="search-results-box">
                
                    <!-- <h3>Arizona, USA</h3> -->
                    <form id="hayme_order_search_form">
                    <div class="orderby">Sort by: <select class="selectbox" name="orderby">
                    								<option value="desc">Price: Descending</option>
                                                    <option value="asc">Price: Ascending</option>
                    							  </select>
                   	</div>
                    <input type="hidden" name="action" value="hayme_order_search_result" />
                    <?php foreach($_GET as $key => $value): ?>
                    <input type="hidden" name="<?php echo $key ?>" value="<?php echo $value ?>" />
                    <?php endforeach; ?>
                    </form>
                    
                    <br />
                    <a href="#" class="sr-edit">edit search</a> <a href="#" class="sr-save">save search</a>
                    <input type="hidden" id="hayme_order_search_result" value="<?php echo $order_search_result ?>"/>
              		<div id="search-results-ordered" style="overflow:scroll">Loading result..</div>
                </div>
                
            </div>
        </div>
        
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">
         			
                    <!-- links -->
                    <div id="page-links">
                    <!--
                    <ul>
                        <li><a href="<?php echo get_permalink(135); ?>" class="selected">Property Details</a></li>
                        <li><a href="<?php echo get_permalink(140); ?>">Photos</a></li>
                        <li><a href="<?php echo get_permalink(142); ?>">Location Map</a></li>
                        <li><a href="<?php echo get_permalink(144); ?>">Agent Profile</a></li>
                        <li><a href="<?php echo get_permalink(148); ?>">Video</a></li>
                    </ul>
                    -->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#property-detail" data-toggle="tab">Property Details</a></li>
                            <li><a href="#photos" data-toggle="tab">Photos</a></li>
                            <li><a href="#location" data-toggle="tab" class="map">Location Map</a></li>
                            <li><a href="#agent" data-toggle="tab">Agent Profile</a></li>
                            <!--<li><a href="#video" data-toggle="tab">Video</a></li>-->
                        </ul>
                    </div>
                    <!-- end links -->

                    <div id="mid-col-main">
                        <div class="featured-image">
                            
                            <div class="tab-content">
                                <!-- property-detail -->
                                <div class="tab-pane active" id="property-detail">
                                    <div class="featured-content">
                                        <div class="s-property-img-big" style="background: url(<?php echo $propertyimg ?>) no-repeat center center; background-size: cover;">
                                            <!-- <img src="" title="big" class="img-responsive" /> -->
                                        </div>
                                        <div class="s-property-map">
                                            <!--<img src="<?php bloginfo('template_directory');?>/img/sr-propertymap-big-img.jpg" title="map" class="img-responsive" />-->
                                            <div class="acf-map">
                                                <div class="marker img-responsive" data-lat="<?php echo (string)$mylisting->Location->Latitude; ?>" data-lng="<?php echo (string)$mylisting->Location->Longitude; ?>"></div>
                                            </div>
                                        </div>
                                        <div class="clearthis"></div>
                                    </div>
                                    <div class="text-content">
                                        <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
                                        <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
                                        <div class="description-tbl noborders">
                                            <span class="item-price">$ <?php echo number_format((int)$mylisting->ListPrice) ?> USD</span>
                                            <select class="selectbox" id="convert-item-price">
                                                <option value="USD">U.S. Dollars</option>     
                                                <option value="SGD">S.G. Dollars</option>
                                            </select>
                                            <input type="hidden" id="current-convertion" value="USD"/>
                                            <input type="hidden" id="current-price" value="<?php echo (string)$mylisting->ListPrice ?>"/>
                                        </div>
                                        <div class="property-detail-box">
                                            <div class="sp-col-1">
                                                <div class="lefty">Community:</div><div class="righty"><?php echo $community_name; ?></div> <div class="clearthis"></div>
                                                <div class="lefty">Bedrooms: </div>     <div class="righty"><?php echo (string)$mylisting->Bedrooms ?></div> <div class="clearthis"></div>
                                                <div class="lefty">Baths: </div>        <div class="righty"><?php echo (string)$mylisting->Bathrooms ?></div> <div class="clearthis"></div>
                                                <div class="lefty">Partial Baths: </div>    <div class="righty"><?php echo (string)$mylisting->PartialBathrooms ?></div> <div class="clearthis"></div>
                                                <div class="lefty">Property Type: </div>   <div class="righty"><?php echo (string)$mylisting->PropertyType ?></div>  <div class="clearthis"></div>
                                            </div>
                                            <div class="sp-col-2">
                                                <div class="lefty">Agent: </div>        <div class="righty"><?php echo $agent['display_name'] ? $agent['display_name'] : 'Not Set' ?></div ><div class="clearthis"></div>
                                                <div class="lefty">&nbsp;</div>     <div class="righty regularfont"><?php echo $agent['agentposition'] ?></div ><div class="clearthis"></div>
                                                <div class="lefty">&nbsp;</div>     <div class="righty regularfont"><?php echo $agent['agentcontact'] ?></div ><div class="clearthis"></div>
                                            </div>
                                            <div class="clearthis"></div>
                                        </div>
                                        <div class="propery-full-desc">
                                            <h3>Property Description:</h3>
                                            <p><?php echo (string)$mylisting->ListingDescription ?></p>
                                        </div>                                    
                                    </div>
                                </div>
                                <!-- end property-detail tab -->

                                <div class="tab-pane" id="photos">
                                    <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
                                    <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
                                    <div class="image-box">
                                        <div id="gallerymainimg">
                                            <div class="picnt">

                                            </div>
                                        </div>
                                        <script>
                                            jQuery(document).ready( function($){
                                                
                                                var imgsrc=$("#wpsimplegallery_container ul li:first-child a").attr('href');

                                                console.log(imgsrc);
                                                $(".picnt").html('<img src="'+imgsrc+'" />');
                                                $("#wpsimplegallery_container ul li a").click( function(e){

                                                    var newsrc=$(this).attr('href');

                                                    console.log(newsrc+" new src");
                                                    jQuery(".picnt").fadeOut('slow',function(){
                                                        jQuery(".picnt img").attr('src',newsrc);
                                                        jQuery(this).fadeIn('slow');
                                                       
                                                    });
                                                    e.preventDefault();
                                                });
                                            });
                                        </script>
                                        <div id="wpsimplegallery_container">
                                            <ul id="wpsimplegallery" class="clearfix">
                                                <?php if(is_array($mylisting->Photos->Photo)): ?>
                                                <?php foreach($mylisting->Photos->Photo as $ph): ?>
                                                <li><a href="<?php echo (string)$ph->MediaURL ?>"><img src="<?php echo (string)$ph->MediaURL ?>" alt="<?php echo basename((string)$ph->MediaURL) ?>" rel="wpsimplegallery_group_184"/></a></li>
                                                <?php endforeach; ?>
                                                <?php else: ?>
                                                <li><a href="<?php echo (string)$mylisting->Photos->Photo->MediaURL ?>"><img src="<?php echo (string)$mylisting->Photos->Photo->MediaURL ?>" alt="<?php echo basename((string)$mylisting->Photos->Photo->MediaURL) ?>" rel="wpsimplegallery_group_184"/></a></li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                        <div class="clearthis"></div>
                                    </div>
                                </div>
                                <!-- end photos tab -->

                                <div class="tab-pane" id="location">
                                    <div class="text-content">
                                        <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
                                        <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
                                    </div>

                                    <div class="acf-map">
                                        <div class="marker" data-lat="<?php echo (string)$mylisting->Location->Latitude; ?>" data-lng="<?php echo (string)$mylisting->Location->Longitude; ?>"></div>
                                    </div>
                                </div>
                                <!-- end location tab -->

                                <div class="tab-pane" id="agent">
                                    <div class="text-content">
                                        <h2 class="community-name"><?php echo (string)$mylisting->ListingTitle ?></h2>
                                        <h3 class="community-address"><?php echo $listing_address->FullStreetAddress ?>, <?php echo $listing_address->City ?>, <?php echo $listing_address->StateOrProvince ?>, <?php echo $listing_address->Country ?></h3>
                                    </div>

                                    <?php //print_r($agent) ?>
                                    <?php if($agent): ?>
                                    <div class="profile-box">
                                        <img src="http://50.87.50.55/~finestg1/wp-content/themes/golf/img/karen-baldwin-profile-pic.jpg" alt="agent" title="agent" class="agentpic">
                                        <div class="profile-description">
                                            <h3 class="agent-name"><?php echo $agent['display_name'] ?></h3>
                                            <div><?php echo $agent['display_name'] ?>,  Designation 2,  Designation 3</div>
                                            <div>(480) 694-0098  |  <a href="#">Email Me</a></div>
                                            <br>
                                            <div>Desert Mountain Real Estate</div>
                                            <div>37700 Desert Mountain Real Estate Pkwy</div>
                                            <div>Scottsdale, AZ 85262</div>
                                            <br>
                                            <div class="special-exp">Years Experience:   <span class="blackthis">15</span></div>
                                            <div class="special-exp">Community Specialist:</div>
                                            <div>- DC Ranch</div>
                                            <div>- Ancala</div>
                                            <div>- Moon Valley</div>
                                        </div>

                                        <div class="clearthis"></div>

                                        <div class="biography-box">
                                            <h4 class="bio">Biography: </h4>

                                            <p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur scilisis at vero eros et accumsa  posuere cubilia urae aecenas condimentum nisl eros, non ornare libero rhoncus iaculis.</p>

                                            <p>Sed eu risus tincidunt, mattis sem non, imperdiet duiorbi tristique senectus e.orem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet ultricies nulla, eu pharetra sem. Maecenas a ligula quam.</p>

                                            <p>Cras sodales nibh id urna euismod mollis. Suspendisse sagittis quam at dapibus commodo. Maecenas vel dictum risus. Nullam sodales a sapien at ornare. Maecenas condimentum nulla quis lacus sodales, ac ornare mi imperdiet. Aenean feugiat tellus a dignissim ornare, phasellus sed imperdiet nibh. 
                                            Phasellus a diam nulla. Mauris accumsan sit amet risus sit amet pharetra. Integer sed urna in quam tempus commodo quis nec ipsum. Nulla cursus ac tortor ut sollicitudin. Donec mattis, urna ac volutpat tempus, ante ipsum aliquet leo, venenatis sollicitudin lacus ligula et mauris. Maecenas mollis erat enim, at semper sem dapibus a. Aliquam viverra ullamcorper urna in adipiscing.</p>

                                            <p>Cras dignissim ipsum sit amet euismod aliquam. Mauris orci augue, tempus non pharetra mollis, iaculis eu sem. Curabitur interdum sapien vitae dolor facilisis, vel sollicitudin libero fermentum. Sed sed turpis quis sapien facilisis tempus. Phasellus posuere at nibh in sollicitudin. Aliquam sed vehicula enim, sit amet luctus est. Proin eget metus egestas dui viverra rutrum eu eget diam. Ut egestas auctor magna non auctor. Proin enim risus, tempus a nibh sed, egestas hendrerit libero. Sed volutpat sed ante nec vehicula. Aliquam viverra, tortor vehicula tincidunt pellentesque, arcu lorem viverra elit, egestas vestibulum tortor sapien non sem. Donec ut tristique dolor.</p>

                                            <p>Nunc vitae ante magna. Donec fringilla nisi at mauris rutrum porttitor. Quisque pretium tempor nulla, vitae ornare magna dignissim eget. Nulla vel quam vel felis sodales luctus. Vestibulum tincidunt mi quis sapien viverra, pellentesque dictum massa tincidunt. Nunc sit amet semper urna, quis gravida elit. Aliquam dictum ullamcorper ante consectetur blandit. Aliquam erat volutpat. Maecenas posuere turpis vel nisi tincidunt suscipit. Vivamus vestibulum vehicula dolor et suscipit. Integer arcu est, ornare sit amet lacinia a, lobortis nec dolor. Etiam lobortis laoreet aliquam. Morbi blandit in nibh eget ullamcorper. Nunc cursus turpis orci, non iaculis purus consectetur ut. Morbi faucibus dolor in dictum mattis. Fusce venenatis massa nunc, et dignissim arcu convallis pellentesque.</p>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <!-- end agent tab -->

                                <!--
                                <div class="tab-pane" id="video">
                                    <?php //get_template_part('includes/tab/community','video-tab-content') ?>
                                </div>
                                -->
                                <!-- end video tab -->

                            </div>
                        </div>
                        <!-- END featuredimage -->
                    </div>
            </div>
        
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
        
        	<div id="sidebar-r">

				            
            	<div class="item-box">
                <a href="#">
                <img src="<?php bloginfo('template_directory');?>/img/contact-icon.png" class="sidebar-r-ico" /> 
                </a>
                <div class="sidebar-r-text">
                 <a href="#">Contact Agent</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/info-icon.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="<?php echo $community_link; ?>">More About Community</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
                
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#" class="get-answer-link" data-toggle="modal" data-target="#getAnswersCommunityModal">Get Answers</a>
                <?php if($_POST['gasubmit']): ?>
                    <script>
                        jQuery(document).ready(function() {
                            jQuery('a.get-answer-link').trigger('click');
                        });
                    </script>
                <?php endif; ?>
                </div>
                
                	<div class="clearthis"></div>
                </div>
            
            
            	 <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon4.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="<?php bloginfo('url'); ?>/community-blog/?id=<?php echo $community_id; ?>">Community Blog</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
            
            
            
            <?php if ( is_user_logged_in() ) { ?>

            <!-- scorecard -->

            <div class="scorecard">
                <div class="scorecard-content">
                    <?php wpfp_property_link(0, "", 0, array( 'listing_key' => ((string)$mylisting->ListingKey) )) ?>
                </div>
            </div>
            
            <?php } else { echo "<br/>";} ?>
            
            
            <!-- more links -->
            
            <div class="more-links">
                <a href="#" onclick="window.print();return false" ><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
                <a href="mailto:<?php //the_field('communityemail')?>"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
                <a href="#" data-toggle="modal" data-target="#shareModal"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
            </div>

            <!-- Share THis  -->
            <?php  share_this_items( $_SERVER['HTTP_HOST']."/property-searchs?listingKey=".$mylisting->ListingKey, "Property" ); ?>
            
            
            <div class="e-agentbt"><a href="#">Email Agent</a></div>
        
        
       	</div>
         
         
         
        </div>
        <!-- end three columns -->

        <!-- Get answers Modal -->
        <!-- Sample Agent Email -->
        <?php $email_array = array('rmlevangelio@gmail.com'); ?>
        <?php get_answers_community_modal($email_array); ?>
        
     
        <div class="clearthis"></div>
        

        
		</div>
    </div>
</div>
<script type="text/javascript">
var addthis_config = {"data_track_clickback":false,"data_track_addressbar":false,"data_track_textcopy":false,"ui_atversion":"300"};
var addthis_product = 'wpp-3.1';
</script><script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=93b61cfb127e9f8afd95a2c8fdae5442"></script>   
<?php get_footer(); ?>
<?php get_header(); ?>


<div id="page-content" class="blogsingle">
	<div class="row">
    	<!-- three columns -->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-col">
         
        	
        </div>
        
            <!-- START MAIN -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">
            <?php
                if ( have_posts() ) : while ( have_posts() ) : the_post(); 
            ?>  
                 <div id="mid-col-main">
                    <div class="img_wrap">
                        <?php the_post_thumbnail('large');  ?>
                    </div>
                     <h1 class="post-title"><?php the_title(); ?></h1>
                     <span class="sbdate"><?php echo get_the_date(); ?> | <?php echo get_the_author();?></span>
                    <?php
                       

                        the_content();

              
                    ?>                   
                    <div class="clearthis"></div>
                </div>
            </div>
            <!-- END MAIN -->
        <?php endwhile; else: ?>
        <?php endif; ?>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
        
        	<div id="sidebar-r">

				            
            	
              
                
                
               
            </div>
       	</div>
         
         
         
        </div>
        <!-- end three columns -->
        
     
        <div class="clearthis"></div>
        
        
        
		</div>
    </div>
</div>

<?php get_footer(); ?><?php
/*

Template Name: Blog Template

*/

?>
<?php get_header(); ?>


<div id="page-content">
    <div class="row">
        <!-- three columns -->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-col">
         
            
        </div>
        
            <!-- START MAIN -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">
            <?php
                if ( have_posts() ) : while ( have_posts() ) : the_post(); 
            ?>  
                 <div id="mid-col-main">
                    <div class="img_wrap">
                        <?php the_post_thumbnail('large');  ?>
                    </div>
                     <h1 class="post-title"><?php the_title(); ?></h1>
                    <?php
                        global $wp_query;

                        the_content();

                        $numbr = 5;

                        $paged = 1;
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                        $args = array(
                                    'posts_per_page'   => 3,
                                    'post_type' => 'post',
                                    'post_status' => 'publish',
                                    'paged' => $paged,
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'post_type'        => 'post',
                                    'post_status'      => 'publish',
                                );
                        $the_query = new WP_Query($args);

                        if($the_query->have_posts()){
                            while ( $the_query->have_posts() ) {

                                $the_query->the_post();
                    ?>
                    <div class="newscontent">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                            <h3><?php the_title(); ?></h3>
                        </a>
                        <span class="sbdate"><?php echo get_the_date(); ?> | <?php echo get_the_author();?></span>
                        <p><?php the_excerpt();?> </p>
                    </div>
                    <div class="clearthis"></div>
                    <?php
                            }//end while
                        }//end if
                    ?>
                    <div class="newspagination">
                    <?php
                        $big = 999999999; // need an unlikely integer
                                            
                          echo paginate_links( array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, get_query_var('paged') ),
                            'show_all' => 'true',
                            'prev_text'    => __('<'),
                            'next_text'    => __('>'),
                            'total' => $the_query->max_num_pages
                          ) );
                            wp_reset_postdata();
                         wp_reset_query();
                    ?>
                    </div>
                    <!-- END PAGINATION -->
                    <div class="clearthis"></div>
                </div>
            </div>
            <!-- END MAIN -->
        <?php endwhile; else: ?>
        <?php endif; ?>
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
        
            <div id="sidebar-r">

                            
                
              
                
                
               
            </div>
        </div>
         
         
         
        </div>
        <!-- end three columns -->
        
     
        <div class="clearthis"></div>
        
        
        
        </div>
    </div>
</div>

<?php get_footer(); ?>

<?php get_header(); ?>

<div id="page-content">
	<div class="row">
            <?php destination_left_section(); ?>
        
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">
         			
                    <div id="page-links">
                        <?php get_template_part('includes/tab/tab','destination-links' ); ?>
                    </div>
                    
                    <!-- end links -->
                    
                    <!-- START MAIN -->
                    <?php if(have_posts()){  while (have_posts() ) { the_post();?>
                          <div id="mid-col-main">
                            <div class="featured-image">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="community">
                                         <?php get_template_part('includes/tab/destination','details-tab-content' );?>
                                    </div> 
                                    <!-- end community tab -->

                                    <div class="tab-pane" id="photos">
                                        <?php get_template_part('includes/tab/destination','photos-tab-content' ); ?>
                                    </div>
                                    <!-- end photos tab -->

                                    <div class="tab-pane" id="location">
                                        <?php get_template_part('includes/tab/destination','location-tab-content' ); ?>
                                    </div>
                                    <!--end location tab -->

                                    <div class="tab-pane" id="video">
                                        <?php get_template_part('includes/tab/destination','video-tab-content' ); ?>
                                    </div>
                                    <!-- end vides tab -->
                                </div> <!-- end tab-content -->                        
                            </div><!-- END featured image -->
                        </div><!-- END MAIN -->


                    <?php
                                }//end while
                            }   //end if
                            wp_reset_postdata();
                            wp_reset_query();
                        ?>
                    <!-- END MAIN -->
            </div>
        
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
        
        	<?php get_sidebar('destination'); ?>
        
        </div>
        <!-- end three columns -->
        
     
        <div class="clearthis"></div>

        <?php destination_modal(); ?>

		</div>
    </div>
</div>

<?php get_footer(); ?>
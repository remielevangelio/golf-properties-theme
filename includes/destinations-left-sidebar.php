	<!-- three columns -->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-col">
         
        	<div class="search-results">
            
            	<div class="sr-headline">
            	<?php if(isset($_GET['dscountry']) && isset($_GET['dsstate']) && isset($_GET['dscity'])) { ?>
                    <h2>Search Results</h2>
                <?php } else { ?>
                    <h2>List of Destinations</h2>
                <?php } ?>
                <div class="sub-head"></div>
                </div>
                
                
                <div id="search-results-box">
                
                    <?php //echo do_shortcode('[ULWPQSF id=278 formtitle=0]'); ?>
                    
                    <?php if(isset($_GET['dscountry']) && isset($_GET['dsstate']) && isset($_GET['dscity'])) { ?>
                    <input type="text" class="save-search-title" style="width: 99%;margin-top: 5px;" placeholder="Name your search..." link="<?php echo $_SERVER['REQUEST_URI']; ?>" />
                    <a href="#" class="sr-edit" data-target="#myModald" data-toggle="modal">edit search</a> <a href="#" class="sr-save">save search</a>
                    <?php } ?>    
                    <div id="search-content">
                        <?php
                                if(isset($_GET['dscountry']) && isset($_GET['dsstate']) && isset($_GET['dscity'])) {
                                $country_id = (int) mysql_real_escape_string(trim($_GET['dscountry']));
                                $state_id = mysql_real_escape_string(trim($_GET['dsstate']));
                                $city_id = (int) mysql_real_escape_string(trim($_GET['dscity']));

                                $args = array(
                                    'showposts' => -1, 
                                    'post_type' => 'destinations',
                                    'meta_query' => array(
                                            array(
                                                'key' => 'destinationlocation2',
                                                'value' => ':"'.$country_id.'";',
                                                'compare' => 'LIKE'
                                            ),
                                            array(
                                                'key' => 'destinationlocation2',
                                                'value' => ':"'.$city_id.'";',
                                                'compare' => 'LIKE'
                                            ),
                                            array(
                                                'key' => 'destinationlocation2',
                                                'value' => ':"'.$state_id.'";',
                                                'compare' => 'LIKE'
                                            ),
                                        ),                               
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'post_status'      => 'publish'
                                        );
                        } else {
                                $args = array(
                                    'showposts' => -1, 
                                    'post_type' => 'destinations',                            
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'post_status'      => 'publish'
                                );
                        }


                                $the_query= new WP_query( $args);
                                //echo "before if";
                               //print_r($the_query);
                                $pstc=1;
                                //echo count($posts);
                                $ctr = 0;
                                 if($the_query->have_posts()){
                                    while ( $the_query->have_posts() ) {
                                        $the_query->the_post();
                                        $ctr++;
                                        //the_field('communitydescription');

                                        //gallery images use the_content();
                                        //the_content();
                             
                        ?>
                        <!-- search item -->
                        <div class="search-item">
                        	<?php
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('small');  
                                    }else{
                                ?>
                                 <img src="<?php bloginfo('template_directory');?>/img/sample-ancala.jpg" class="img-responsive"/>
                                 <?php
                                    }
                                ?>
                            
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                            <div class="sr-desc">
                                <?php 
                                    $desc=get_field('destinationdescription');
                                    $position = stripos ($desc, "."); //find first dot position

                                    if($position) { //if there's a dot in our soruce text do
                                        $offset = $position + 1; //prepare offset
                                        $position2 = stripos ($desc, ".", $offset); //find second dot using offset
                                        $first_two = substr($desc, 0, $position2); //put two first sentences under $first_two

                                        echo $first_two . '.'; //add a dot
                                    }

                                    else {  //if there are no dots
                                        //do nothing
                                    }
                                ?> 
                            </div>
                        
                        </div>
                        <!-- end search item -->
                    
                        <?php
                                }//end while
                            }   //end if
                            wp_reset_postdata();
                            wp_reset_query();
                        ?>
                            <span style="display: none;" class="search-count2"><?php echo $ctr; ?> matches</span>
                            <script>
                                jQuery(document).ready(function($) {
                                    var count = $('.search-count2').html();
                                    $('.sr-headline .sub-head').html(count);
                                });
                            </script>
                	</div>
                
                
                </div>
                
            </div>
        </div>

<?php destination_modal(); ?>
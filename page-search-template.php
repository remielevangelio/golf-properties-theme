<?php
/*

Template Name: Search Template

*/

?>
<?php get_header(); ?>


<div id="page-content" class="searchtemplate">
    <div class="row">
        <!-- START MAIN -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mid-col">
        <?php
            if ( have_posts() ) : while ( have_posts() ) : the_post(); 
        ?>  
            <div id="mid-col-main">
                <h1 class="post-title"><?php the_title(); ?></h1>
                <?php

                            the_content();
                ?>
                <div class="btn"><a href="#" data-toggle="modal" data-target="#myModalp">Property Search &rarr;</a></div><br /><br />
                <div class="btn"><a href="#" data-toggle="modal" data-target="#myModald">Destinations Search &rarr;</a></div><br /><br />
                <div class="btn"><a href="#" data-toggle="modal" data-target="#myModalg">Golf Community Search &rarr;</a></div><br /><br />
                <div class="btn"><a href="#" data-toggle="modal" data-target="#myModala">Agent Search &rarr;</a></div><br /><br />
                <div class="btn"><a href="#" data-toggle="modal" data-target="#myModalc">Contact Agent &rarr;</a></div>
                
                
                <!-- END PAGINATION -->
                <div class="clearthis"></div>
            </div>
        </div>
        <!-- END MAIN -->
    <?php endwhile; else: ?>
    <?php endif; ?>
    </div>
    <!-- end three columns -->
        
     
    <div class="clearthis"></div>



</div>

<!-- property modal -->
<div id="myModalp" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content boxtype">
            <div class="modal-header noborders">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title mypopuptitle" id="myModalLabel">Property Search</h4>
                <h5 class="modal-sub-title">Please complete the following to start your search:</h5>
            </div>
            <div class="modal-body">
                <div id="contact-popup">
                    <form action="/property-search/">
                        <div class="cdsel" id="cdcountry">
                            <div class="left-col">
                                <p><label>Country</label></p>
                            </div>
                            <div class="right-col">
                                <select id="pscountry" name="pscountry" class="psst">
                                    <option value="">Select Country</option>
                                    <?php 

                                        $countries = rem_get_countries(); 
                                        foreach($countries as $country => $val ) {
                                    ?>
                                        <option value="<?php echo $country; ?>"><?php echo $val; ?></option>
                                    <?php
                                        }

                                    ?>



                                </select>
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <div class="cdsel" id="cdstate">
                            <div class="left-col">
                                <p><label>State</label></p>
                            </div>
                            <div class="right-col">
                                <select id="psstate" name="psstate" class="psst">
                                    <option value="">Select State</option>
                                    <option value="ca">California</option>
                                    <option value="az">Arizona</option>
                                </select>                            
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <div class="cdsel" id="cdcity">
                            <div class="left-col">
                                <p><label>City</label></p>
                            </div>
                            <div class="right-col">
                                <select id="pscity" name="pscity" class="psst">
                                    <option value="">Select City</option>
                                    <option value="la">L.A.</option>
                                    <option value="sf">San Francisco</option>
                                </select>                           
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <div class="cdsel" id="cdptype">
                            <div class="left-col">
                                <p><label>Property Type</label></p>
                            </div>
                            <div class="right-col">
                                <?php $property_types = hayme_get_property_types(); ?>
                                <select id="pstype" name="pstype" class="psst">
                                    <option value="">Select Property Type</option>
                                    <?php if($property_types): ?>
                                    <?php foreach($property_types as $types): ?>
                                    <option value="<?php echo strtolower($types) ?>"><?php echo $types ?></option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>                           
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <div class="" id="cdboxes">
                            <div class="left-col">
                                <p><label> </label></p>
                            </div>
                            <div class="gdtb right-col">
                                <div class="left-col">
                                    <input type="text" name="min-price" placeholder="Minimum Price" value="">
                                </div>  
                                <div class="right-col">                             
                                    <input type="text" name="max-price" placeholder="Maximum Price" value="">
                                </div>
                                <div class="clearthis"></div> 

                                <div class="left-col"> 
                                    <input type="text" name="beds" placeholder="Bedrooms" value="">
                                </div>
                                <div class="right-col">                         
                                    <input type="text" name="baths" placeholder="Bathrooms" value="">
                                </div>                         
                                <div class="clearthis"></div> 
                                
                                <div class="left-col">
                                    <input type="text" name="min-sq-meter" placeholder="Minimum Sq. Ft." value="">
                                </div>
                                <div class="right-col">
                                    <input type="submit" value="Submit →" class="wpcf7-form-control wpcf7-submit s-des btn">
                                </div>
                                <div class="clearthis"></div> 
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>
                    </form>
                    <div class="clearthis"></div>
                </div>
            </div>
        </div>
    </div>  
</div>
<!-- end modal property search -->

<!-- destinations modal -->
<div id="myModald" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content boxtype">
            <div class="modal-header noborders">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title mypopuptitle" id="myModalLabel">Destinations Search</h4>
                <h5 class="modal-sub-title">Please complete the following to start your search:</h5>
            </div>
            <div class="modal-body">
                <div id="contact-popup">
                    <form action="<?php bloginfo('url') ?>/golf-destinations/" method="GET">
                        <div class="cdsel" id="dscountryc">
                            <div class="left-col">
                                <p><label>Country</label></p>
                            </div>
                            <div class="right-col">
                                <select id="dscountry" name="dscountry" class="psst">
                                    <option value="">Select Country</option>
                                    <?php 

                                        $countries = rem_get_countries(); 
                                        foreach($countries as $country => $val ) {
                                    ?>
                                        <option value="<?php echo $country; ?>"><?php echo $val; ?></option>
                                    <?php
                                        }

                                    ?>
                                    <!-- <option value="us">United States</option>
                                    <option value="ca">Canada</option> -->

                                    <!-- DISPLAY ALL COUNTRY ON EVERY DESTINATION -->
                                    
                                </select>
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <div class="cdsel" id="dsstatec">
                            <div class="left-col">
                                <p><label>State</label></p>
                            </div>
                            <div class="right-col">
                                <select id="dsstate" name="dsstate" class="psst">
                                    <option value="">Select State</option>
                                </select>                            
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <div class="cdsel" id="dscityc">
                            <div class="left-col">
                                <p><label>City</label></p>
                            </div>
                            <div class="right-col">
                                <select id="dscity" name="dscity" class="psst">
                                    <option value="">Select City</option>
                                    <option value="la">L.A.</option>
                                    <option value="sf">San Francisco</option>
                                </select>                           
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>
                        
                        <div class="" id="dsboxesc">
                            <div class="left-col">
                                <p><label> </label></p>
                            </div>
                            <div class="gdtb right-col">                                
                                <div class="left-col">
                                    <input type="submit" value="Submit →" class="wpcf7-form-control wpcf7-submit s-des btn fleft">
                                </div>
                                <div class="clearthis"></div> 
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>
                    </form>
                    <div class="clearthis"></div>
                </div>
            </div>
        </div>
    </div>  
</div>
<!-- end modal destinations search -->

<!-- community modal -->
<div id="myModalg" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content boxtype">
            <div class="modal-header noborders">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title mypopuptitle" id="myModalLabel">Golf Community Search</h4>
                <h5 class="modal-sub-title">Please complete the following to start your search:</h5>
            </div>
            <div class="modal-body">
                <div id="contact-popup">
                    <form action="<?php bloginfo('url') ?>/golf-communities/" method="GET">
                        <div class="cdsel" id="gcountryc">
                            <div class="left-col">
                                <p><label>Country</label></p>
                            </div>
                            <div class="right-col">
                                <select id="gcountry" name="gcountry" class="psst">
                                    <option value="">Select Country</option>
                                    <?php 

                                        $countries = rem_get_countries(); 
                                        foreach($countries as $country => $val ) {
                                    ?>
                                        <option value="<?php echo $country; ?>"><?php echo $val; ?></option>
                                    <?php
                                        }

                                    ?>
                                </select>
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <div class="cdsel" id="gstatec">
                            <div class="left-col">
                                <p><label>State</label></p>
                            </div>
                            <div class="right-col">
                                <select id="gstate" name="gstate" class="psst">
                                    <option value="">Select State</option>
                                    <option value="ca">California</option>
                                    <option value="az">Arizona</option>
                                </select>                            
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <div class="cdsel" id="gcityc">
                            <div class="left-col">
                                <p><label>City</label></p>
                            </div>
                            <div class="right-col">
                                <select id="gcity" name="gcity" class="psst">
                                    <option value="">Select City</option>
                                    <option value="la">L.A.</option>
                                    <option value="sf">San Francisco</option>
                                </select>                           
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <!-- <div class="cdsel" id="gcommunityc">
                            <div class="left-col">
                                <p><label>Community</label></p>
                            </div>
                            <div class="right-col">
                                <select id="gcommunity" name="gcommunity" class="psst">
                                    <option value="">Select Community</option>
                                    <option value="co">Etherial Community</option>
                                    <option value="ho">Etherial Community</option>
                                </select>                           
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div> -->

                        <div class="" id="gboxesc">
                            <div class="left-col">
                                <p><label> </label></p>
                            </div>
                            <div class="gdtb right-col">
                               
                                <div class="left-col">
                                    <input type="submit" value="Submit →" class="wpcf7-form-control wpcf7-submit s-des btn fleft">
                                </div>
                                <div class="clearthis"></div> 
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>
                    </form>
                    <div class="clearthis"></div>
                </div>
            </div>
        </div>
    </div>  
</div>
<!-- end modal community search -->

<!-- agent modal -->
<div id="myModala" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content boxtype">
            <div class="modal-header noborders">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title mypopuptitle" id="myModalLabel">Agent Search</h4>
                <h5 class="modal-sub-title">Please complete the following to start your search:</h5>
            </div>
            <div class="modal-body">
                <div id="contact-popup">
                    <form action="<?php bloginfo('url') ?>/agents/" method="GET">
                        <div class="cdsel" id="acountryc">
                            <div class="left-col">
                                <p><label>Country</label></p>
                            </div>
                            <div class="right-col">
                                <select id="acountry" name="acountry" class="psst">
                                    <option value="">Select Country</option>
                                    <?php 

                                        $countries = rem_get_countries(); 
                                        foreach($countries as $country => $val ) {
                                    ?>
                                        <option value="<?php echo $country; ?>"><?php echo $val; ?></option>
                                    <?php
                                        }

                                    ?>
                                </select>
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <div class="cdsel" id="astatec">
                            <div class="left-col">
                                <p><label>State</label></p>
                            </div>
                            <div class="right-col">
                                <select id="astate" name="astate" class="psst">
                                    <option value="">Select State</option>
                                    <option value="ca">California</option>
                                    <option value="az">Arizona</option>
                                </select>                            
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <div class="cdsel" id="acityc">
                            <div class="left-col">
                                <p><label>City</label></p>
                            </div>
                            <div class="right-col">
                                <select id="acity" name="acity" class="psst">
                                    <option value="">Select City</option>
                                    <option value="la">L.A.</option>
                                    <option value="sf">San Francisco</option>
                                </select>                           
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>

                        <!-- <div class="cdsel" id="acommunityc">
                            <div class="left-col">
                                <p><label>Community</label></p>
                            </div>
                            <div class="right-col">
                                <select id="acommunity" name="acommunity" class="psst">
                                    <option value="">Select Community</option>
                                    <option value="co">Etherial Community</option>
                                    <option value="ho">Etherial Community</option>
                                </select>                           
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div> -->

                        <div class="" id="aboxesc">
                            <div class="left-col">
                                <p><label> </label></p>
                            </div>
                            <div class="gdtb right-col">
                               
                                <div class="left-col">
                                    <input type="submit" value="Submit →" class="wpcf7-form-control wpcf7-submit s-des btn fleft">
                                </div>
                                <div class="clearthis"></div> 
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>
                    </form>
                    <div class="clearthis"></div>
                </div>
            </div>
        </div>
    </div>  
</div>
<!-- end modal agent search -->

<?php
//contact agent mail function
if(isset($_POST)){
    $caname = trim($_POST['caname']);
    $cname = trim($_POST['cname']);
    $cemail = trim($_POST['cemail']);
    $cphone = trim($_POST['cphone']);
    $cmessage = trim($_POST['cmessage']);

    $to = $caname;
    $subject = 'Finest Golf';
    $message = '';
    $message .= '<p>Name: '.$cname.'</p>';
    $message .= '<p>Email: '.$cemail.'</p>';
    $message .= '<p>Phone: '.$cphone.'</p>';
    $message .= '<p>Message: '.$cmessage.'</p>';

    add_filter( 'wp_mail_content_type', 'set_content_type' );
    function set_content_type( $content_type ){
        return 'text/html';
    }
    wp_mail( $to, $subject, $message );
}

?>

<!-- contact agent modal -->
<div id="myModalc" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content boxtype">
            <div class="modal-header noborders">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title mypopuptitle" id="myModalLabel">Contact Agent</h4>
                <h5 class="modal-sub-title">Please complete the following to start your search:</h5>
            </div>
            <div class="modal-body">
                <div id="contact-popup">
                    <form action="<?php the_permalink(); ?>" method="post">
                        <div class="cdsel" id="canamec">
                            <div class="left-col">
                                <p><label>Agent Name</label></p>
                            </div>
                            <div class="right-col">
                                <select id="caname" name="caname" class="psst">
                                    <option value="">Select Agent</option>
                                    <?php 

                                        $args = array(
                                            'role' => 'aamrole_53a16309a9826',
                                        );

                                        $agent_query2= new WP_User_Query( $args);

                                        if(! empty( $agent_query2->results )) {

                                            foreach($agent_query2->results as $user2) {
                                                $agent_email = get_the_author_meta('user_email',$user2->ID);

                                                echo "<option value='".$agent_email."'>".$user2->display_name."</option>";
                                            }
                                        } else {
                                            echo "<option>-No Agents Available-</option>";
                                        }



                                    ?>
                                </select>
                            </div>
                            <div class="clearthis"></div>
                        </div>
                        <div class="clearthis"></div>
                        <div class="cdsel" id="cnamec">
                            <div class="left-col">
                                <p><label>Your Name</label></p>
                            </div>
                            <div class="right-col">
                                <input type="text" name="cname" id="cname" required>                        
                            </div>
                            <div class="clearthis"></div>

                             <div class="left-col">
                                <p><label>Your Email</label></p>
                            </div>
                            <div class="right-col">
                                <input type="text" name="cemail" id="cemail" required>                        
                            </div>
                            <div class="clearthis"></div>

                            <div class="left-col">
                                <p><label>Your Phone #</label></p>
                            </div>
                            <div class="right-col">
                                <input type="text" name="cphone" id="cphone" required>                        
                            </div>
                            <div class="clearthis"></div>

                            <div class="left-col">
                                <p><label>Message</label></p>
                            </div>
                            <div class="right-col">
                                <textarea name="cmessage" id="cmessage"></textarea>              
                            </div>
                            <div class="clearthis"></div>
                        
                            <div class="left-col">
                                <p><label> </label></p>
                            </div>
                            <div class="gdtb right-col">
                               
                                <div class="left-col">
                                    <input type="submit" value="Submit →" class="wpcf7-form-control wpcf7-submit s-des btn fleft">
                                </div>
                                <div class="clearthis"></div> 
                            </div>
                            <div class="clearthis"></div>
                        </div>

                        <div class="clearthis"></div>
                    </form>
                    <div class="clearthis"></div>
                </div>
            </div>
        </div>
    </div>  
</div>
<!-- end modal contact agent -->

<?php get_footer(); ?>
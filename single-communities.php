<?php get_header(); ?>

<div id="page-content">
	<div class="row">
    	<!-- three columns -->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-col">
         
        	<div class="search-results">
            
            	<?php community_left_section(); ?>
                
            </div>
        </div>
        
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">
         			
                    <div id="page-links">
                        <?php get_template_part('includes/tab/tab','links' );?>
                    </div>
                    
                    <!-- end links -->
                    <?php $pstc=1; if(have_posts()){ while ( have_posts() ) { the_post();  

                                community_main_content();


                            }//end while
                        }   //end if
                        wp_reset_postdata();
                        wp_reset_query();
                    ?>
            </div>
        
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
        
        	<?php get_sidebar('community'); ?>
        
        </div>
        <!-- end three columns -->
        
     
        <div class="clearthis"></div>

        
		</div>
    </div>
</div>



<?php community_modal(); ?>

<?php get_footer(); ?>
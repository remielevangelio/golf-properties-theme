<?php
/*

Template Name: Property Search Agent Template

*/

?>
<?php get_header(); ?>

<div id="page-content">
	<div class="row">
    	<!-- three columns -->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-col">
         
        	<div class="search-results">
            
            	<div class="sr-headline">
            	<h2>Search Results</h2>
                <div class="sub-head">200 matches</div>
                </div>
                
                
                
                <div id="search-results-box">
                
                    <!-- <h3>Arizona, USA</h3> -->
                    
                    <div class="orderby">Sort by: <select class="selectbox">
                    								<option>Price: Descending</option>
                                                    <option>Price: Ascending</option>
                    							  </select>
                   	</div>
                    
                    <br />
                    <a href="#" class="sr-edit">edit search</a> <a href="#" class="sr-save">save search</a>
              		
                    <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample1.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 9,000,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample2.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 7,500,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample3.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 7,450,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample4.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 7,100,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample5.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 6,600,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample6.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 6,500,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample7.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 6,000,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample8.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 5,500,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample1.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 5,000,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                   
                 
                
                
                </div>
                
            </div>
        </div>
        
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">
         			
                    <div id="page-links">
                    <!-- links -->
                    <ul>
                        <li><a href="<?php echo get_permalink(135); ?>">Property Details</a></li>
                        <li><a href="<?php echo get_permalink(140); ?>">Photos</a></li>
                        <li><a href="<?php echo get_permalink(142); ?>">Location Map</a></li>
                        <li><a href="<?php echo get_permalink(144); ?>" class="selected">Agent Profile</a></li>
                        <li><a href="<?php echo get_permalink(148); ?>">Video</a></li>
                    </ul>
                    </div>
                    
                    <!-- end links -->
                    
                    
                    <div id="mid-col-main">
                        <div class="featured-image">

                            
                            <div class="text-content">
                            <h2 class="community-name">9820 E Sterling Ridge Rd</h2>
                            <h3 class="community-address no-margin no-padding">Scottsdale, Arizona, USA</h3>
                            </div>
                            
                            <div class="profile-box">
                            	<img src="<?php bloginfo('template_directory');?>/img/karen-baldwin-profile-pic.jpg" alt="agent" title="agent" class="agentpic" />
                            	
                                <div class="profile-description">
                                <h3 class="agent-name">Karen Baldwin</h3>
                                <div>Associate Broker,  Designation 2,  Designation 3</div>
                                <div>(480) 694-0098  |  <a href="#">Email Me</a></div>
                                <br />
                                <div>Desert Mountain Real Estate</div>
                                <div>37700 Desert Mountain Real Estate Pkwy</div>
                                <div>Scottsdale, AZ 85262</div>
                                <br />
                                <div class="special-exp">Years Experience:   <span class="blackthis">15</span></div>
                                <div class="special-exp">Community Specialist:</div>
                                <div>- DC Ranch</div>
                                <div>- Ancala</div>
                                <div>- Moon Valley</div>
                                </div>
                                
                                <div class="clearthis"></div>
                                
                                <div class="biography-box">
                                	<h4 class="bio">Biography: </h4>

<p>Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur scilisis at vero eros et accumsa  posuere cubilia urae aecenas condimentum nisl eros, non ornare libero rhoncus iaculis.</p>

<p>Sed eu risus tincidunt, mattis sem non, imperdiet duiorbi tristique senectus e.orem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet ultricies nulla, eu pharetra sem. Maecenas a ligula quam.</p>

<p>Cras sodales nibh id urna euismod mollis. Suspendisse sagittis quam at dapibus commodo. Maecenas vel dictum risus. Nullam sodales a sapien at ornare. Maecenas condimentum nulla quis lacus sodales, ac ornare mi imperdiet. Aenean feugiat tellus a dignissim ornare, phasellus sed imperdiet nibh. 
Phasellus a diam nulla. Mauris accumsan sit amet risus sit amet pharetra. Integer sed urna in quam tempus commodo quis nec ipsum. Nulla cursus ac tortor ut sollicitudin. Donec mattis, urna ac volutpat tempus, ante ipsum aliquet leo, venenatis sollicitudin lacus ligula et mauris. Maecenas mollis erat enim, at semper sem dapibus a. Aliquam viverra ullamcorper urna in adipiscing.</p>

<p>
Cras dignissim ipsum sit amet euismod aliquam. Mauris orci augue, tempus non pharetra mollis, iaculis eu sem. Curabitur interdum sapien vitae dolor facilisis, vel sollicitudin libero fermentum. Sed sed turpis quis sapien facilisis tempus. Phasellus posuere at nibh in sollicitudin. Aliquam sed vehicula enim, sit amet luctus est. Proin eget metus egestas dui viverra rutrum eu eget diam. Ut egestas auctor magna non auctor. Proin enim risus, tempus a nibh sed, egestas hendrerit libero. Sed volutpat sed ante nec vehicula. Aliquam viverra, tortor vehicula tincidunt pellentesque, arcu lorem viverra elit, egestas vestibulum tortor sapien non sem. Donec ut tristique dolor.</p>

<p>
Nunc vitae ante magna. Donec fringilla nisi at mauris rutrum porttitor. Quisque pretium tempor nulla, vitae ornare magna dignissim eget. Nulla vel quam vel felis sodales luctus. Vestibulum tincidunt mi quis sapien viverra, pellentesque dictum massa tincidunt. Nunc sit amet semper urna, quis gravida elit. Aliquam dictum ullamcorper ante consectetur blandit. Aliquam erat volutpat. Maecenas posuere turpis vel nisi tincidunt suscipit. Vivamus vestibulum vehicula dolor et suscipit. Integer arcu est, ornare sit amet lacinia a, lobortis nec dolor. Etiam lobortis laoreet aliquam. Morbi blandit in nibh eget ullamcorper. Nunc cursus turpis orci, non iaculis purus consectetur ut. Morbi faucibus dolor in dictum mattis. Fusce venenatis massa nunc, et dignissim arcu convallis pellentesque. 
</p>

                                </div>
                            </div>
                            
                            </div>
                    </div>
            </div>
        
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
        
        	<div id="sidebar-r">

				            
            	<div class="item-box">
                <a href="#">
                <img src="<?php bloginfo('template_directory');?>/img/contact-icon.png" class="sidebar-r-ico" /> 
                </a>
                <div class="sidebar-r-text">
                 <a href="#">Contact Agent</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/info-icon.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#">More About Community</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
                
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#">Get Answers</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
            
            
            	 <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon4.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#">Community Blog</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
            
            
            
            
            <!-- scorecard -->
            
            <div class="scorecard">
            	<div class="scorecard-content">
            		<a href="#">Save To My Scorecard</a>
                </div>
            </div>
            
            
            <!-- more links -->
            
            <div class="more-links">
            	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
            </div>
            
            
            <div class="e-agentbt"><a href="#">Email Agent</a></div>
        
        
       	</div>
         
         
         
        </div>
        <!-- end three columns -->
        
     
        <div class="clearthis"></div>
        
        
        
		</div>
    </div>
</div>

<?php get_footer(); ?>
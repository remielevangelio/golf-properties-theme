<?php
/*

Template Name: Property Search Video Template

*/

?>
<?php get_header(); ?>

<div id="page-content">
	<div class="row">
    	<!-- three columns -->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-col">
         
        	<div class="search-results">
            
            	<div class="sr-headline">
            	<h2>Search Results</h2>
                <div class="sub-head">200 matches</div>
                </div>
                
                
                
                <div id="search-results-box">
                
                    <!-- <h3>Arizona, USA</h3> -->
                    
                    <div class="orderby">Sort by: <select class="selectbox">
                    								<option>Price: Descending</option>
                                                    <option>Price: Ascending</option>
                    							  </select>
                   	</div>
                    
                    <br />
                    <a href="#" class="sr-edit">edit search</a> <a href="#" class="sr-save">save search</a>
              		
                    <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample1.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 9,000,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample2.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 7,500,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample3.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 7,450,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample4.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 7,100,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample5.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 6,600,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample6.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 6,500,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample7.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 6,000,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample8.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 5,500,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                    
                      <!--- search item -->
                    <div class="search-item-b">
                    	
                        <img src="<?php bloginfo('template_directory');?>/img/sr-property-sample1.jpg" class="s-property-thumb img-responsive" />
                       
                       	<div class="s-property-desc">
                                <p><div class="s-property-title">$ 5,000,000</div></p>
                                <p>6 Beds</p>
                                <p>8 Full Baths</p>
                                <p>1 Part Baths</p>
                                <p>11,758 SqFt</p>
                        </div>
                        
                        <div class="clearthis"></div>
                       
                    
                    </div>
                    
                    <!-- end search item -->
                   
                 
                
                
                </div>
                
            </div>
        </div>
        
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">
         			
                    <div id="page-links">
                    <!-- links -->
                    <ul>
                         <li><a href="<?php echo get_permalink(135); ?>">Property Details</a></li>
                        <li><a href="<?php echo get_permalink(140); ?>">Photos</a></li>
                        <li><a href="<?php echo get_permalink(142); ?>">Location Map</a></li>
                        <li><a href="<?php echo get_permalink(144); ?>">Agent Profile</a></li>
                        <li><a href="<?php echo get_permalink(148); ?>" class="selected">Video</a></li>
                    </ul>
                    </div>
                    
                    <!-- end links -->
                    
                    
                    <div id="mid-col-main">
                        <div class="featured-image">

                            
                            <div class="text-content">
                            <h2 class="community-name">9820 E Sterling Ridge Rd</h2>
                            <h3 class="community-address">Scottsdale, Arizona, USA</h3>
                            </div>
                            
                            <img src="<?php bloginfo('template_directory');?>/img/vid1.jpg" alt="video" title="title" class="img-responsive" />
                            <br />
                            <img src="<?php bloginfo('template_directory');?>/img/vid2.jpg" alt="video" title="title" class="img-responsive" />
                            
                            </div>
                    </div>
            </div>
        
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
        
        	<div id="sidebar-r">

				            
         <div class="item-box">
                <a href="#">
                <img src="<?php bloginfo('template_directory');?>/img/contact-icon.png" class="sidebar-r-ico" /> 
                </a>
                <div class="sidebar-r-text">
                 <a href="#">Contact Agent</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/info-icon.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#">More About Community</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
                
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#">Get Answers</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
            
            
            	 <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon4.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#">Community Blog</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
            
            
            
            
            <!-- scorecard -->
            
            <div class="scorecard">
            	<div class="scorecard-content">
            		<a href="#">Save To My Scorecard</a>
                </div>
            </div>
            
            
            <!-- more links -->
            
            <div class="more-links">
            	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
            </div>
            
            
            <div class="e-agentbt"><a href="#">Email Agent</a></div>
        
        
       	</div>
         
         
         
        </div>
        <!-- end three columns -->
        
     
        <div class="clearthis"></div>
        
        
        
		</div>
    </div>
</div>

<?php get_footer(); ?>
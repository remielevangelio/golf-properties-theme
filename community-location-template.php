<?php
/*

Template Name: Community Location Template

*/

?>
<?php get_header(); ?>

<div id="page-content">
	<div class="row">
    	<!-- three columns -->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-col">
         
        	<div class="search-results">
            
            	<div class="sr-headline">
            	<h2>Search Results</h2>
                <div class="sub-head">19 matches</div>
                </div>
                
                
                <div id="search-results-box">
                
                    <h3>Arizona, USA</h3>
                    
                    <a href="#" class="sr-edit">edit search</a> <a href="#" class="sr-save">save search</a>
                   
                    <?php
                        $args = array(
                            'showposts' => -1, 
                            'post_type' => 'communities',                            
                            'orderby'          => 'post_date',
                            'order'            => 'DESC',
                            'post_status'      => 'publish'
                                );
                            $the_query= new WP_query( $args);
                            //echo "before if";
                           //print_r($the_query);
                            $pstc=1;
                            //echo count($posts);
                             if($the_query->have_posts()){
                                while ( $the_query->have_posts() ) {
                                    $the_query->the_post();
                                    //the_field('communitydescription');

                                    //gallery images use the_content();
                                    //the_content();
                         
                    ?>
                      <!-- search item -->
                    <div class="search-item">
                        
                        <?php
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail('small');  
                            }else{
                        ?>
                         <img src="<?php bloginfo('template_directory');?>/img/sample-community-pic.jpg" class="img-responsive" />
                         <?php
                            }
                        ?>
                        <h4><a href="#"><?php the_title(); ?></a></h4>
                        <div class="sr-desc">
                             <?php 
                                $desc=get_field('communitydescription');
                                $position = stripos ($desc, "."); //find first dot position

                                if($position) { //if there's a dot in our soruce text do
                                    $offset = $position + 1; //prepare offset
                                    $position2 = stripos ($desc, ".", $offset); //find second dot using offset
                                    $first_two = substr($desc, 0, $position2); //put two first sentences under $first_two

                                    echo $first_two . '.'; //add a dot
                                }

                                else {  //if there are no dots
                                    //do nothing
                                }
                            ?> 
                        </div>
                    
                    </div>
                    <!-- end search item -->
                
                    <?php
                            }//end while
                        }   //end if
                        wp_reset_postdata();
                        wp_reset_query();
                    ?>
                
                </div>
                
            </div>
        </div>
        
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">
         			
                    <div id="page-links">
                    <!-- links -->
                    <ul>
                    <li><a href="<?php echo get_permalink(56); ?>">Community Details</a></li>
                    <li><a href="<?php echo get_permalink(154); ?>">Photos</a></li>
                    <li><a href="<?php echo get_permalink(156); ?>" class="selected">Location Map</a></li>
                    <li><a href="<?php echo get_permalink(159); ?>">Video</a></li>
                    </ul>
                    </div>
                    <div class="clearthis"></div>
                    
                    <!-- end links -->
                    <?php
                        $args = array(
                            'showposts' => 1, 
                            'post_type' => 'communities',                            
                            'orderby'          => 'post_date',
                            'order'            => 'DESC',
                            'post_status'      => 'publish'
                                );
                            $the_query= new WP_query( $args);
                            //echo "before if";
                           //print_r($the_query);
                            $pstc=1;
                            //echo count($posts);
                             if($the_query->have_posts()){
                                while ( $the_query->have_posts() ) {
                                    $the_query->the_post();
                                    //the_field('communitydescription');

                                    //gallery images use the_content();
                                    //the_content();
                         
                    ?>
                 <div id="mid-col-main">
                        <div class="featured-image">

                            
                            <div class="text-content">
                            <h2 class="community-name"><?php the_title();?></h2>
                            <h3 class="community-address"><?php the_field('communitylocation');?></h3>
                            
                           <div class="image-box">
								<style>
                                  #map_canvas {
                                    width: 535px;
                                    height: 450px;
                                    max-height: 450px;
                                    max-width: 535px;
                                  }
                                </style>
                                <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
                                <!--img src="<?php bloginfo('template_directory');?>/img/dummy-map.jpg" class="img-responsive" title="map" alt="map" /--> 
                                <?php
                                    $map= get_field('communitylocationmap');
                                    $lat=$map['lat'];
                                    $lon=$map['lng'];
                                    $location=get_field('communitylocation');
                                    //print_r($map);
                                ?>                         
                                <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
                                <div style="overflow:hidden;height:500px;width:100%;">
                                    <div id="gmap_canvas" style="height:500px;width:100%;"></div>
                                    <a class="google-map-code" href="http://www.map-embed.com" id="get-map-data">http://www.map-embed.com</a>
                                    <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
                                    </div>
                                    <script type="text/javascript"> 
                                        function init_map(){
                                            var myOptions = {zoom:8,
                                                center:new google.maps.LatLng(<?php echo $lat;?>,<?php echo $lon;?>),
                                                mapTypeId: google.maps.MapTypeId.ROADMAP};
                                                map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
                                                marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(<?php echo $lat;?>, <?php echo $lon;?>)});
                                                infowindow = new google.maps.InfoWindow({content:"<?php echo $location;?>" });
                                                google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});
                                                infowindow.open(map,marker);}
                                                google.maps.event.addDomListener(window, 'load', init_map);
                                    </script>
                           </div>
                               
                        
                            
                            
                            
                        </div>
                         
                        
                    </div>
                </div>
               <?php
                            }//end while
                        }   //end if
                        wp_reset_postdata();
                        wp_reset_query();
                    ?>   
            </div>
        
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
        
        	<div id="sidebar-r">

				            
            	<div class="item-box">
                <a href="#">
                <img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon1.png" class="sidebar-r-ico" /> 
                </a>
                <div class="sidebar-r-text">
                 <a href="#">Search All Properties
                in this Golf Community
                </a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon2.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#">View Agents Specialized in this Golf Community</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
                
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#">Get Answers</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
            
            
            	 <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon4.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#">Community Blog</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
            
            
            
            
            <!-- scorecard -->
            
            <div class="scorecard">
            	<div class="scorecard-content">
            		<a href="#">Save To My Scorecard</a>
                </div>
            </div>
            
            
            <!-- more links -->
            
            <div class="more-links">
            	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
            </div>
        
        
       	</div>
         
         
         	<!-- experts -->
   
        <div id="experts-box">
        	<div class="title-box">
        	<h2 >DC Ranch Experts</h2>
        	</div>
        
        	<div class="item-expert">
            	
                <img src="<?php bloginfo('template_directory');?>/img/expert-1.jpg" class="expert-img" />
                <div class="expert-desc">
                	<h3>Minim veniam</h3>
                    123-456-789
                    Lorem Ipsum
                    <br />
                    <a href="#">View more details »</a>
                </div>
                
                <div class="clearthis"></div>
            
            </div>
            
            
            
            	<div class="item-expert">
            	
                <img src="<?php bloginfo('template_directory');?>/img/expert-2.jpg" class="expert-img" />
                <div class="expert-desc">
                	<h3>Suscip Lorits</h3>
                    123-456-789
                    Lorem Ipsum
                    <br />
                    <a href="#">View more details »</a>
                </div>
                
                <div class="clearthis"></div>
            
           		</div>
                
                
                <div class="item-expert">
            	
                <img src="<?php bloginfo('template_directory');?>/img/expert-3.jpg" class="expert-img" />
                <div class="expert-desc">
                	<h3>Lorebore Mith</h3>
                    123-456-789
                    Lorem Ipsum
                    <br />
                    <a href="#">View more details »</a>
                </div>
                
                <div class="clearthis"></div>
            
           		</div>
            
        </div>
        
        
        <!-- end experts -->
        
        </div>
        <!-- end three columns -->
        
     
        <div class="clearthis"></div>
        
        
        
		</div>
    </div>
</div>

<?php get_footer(); ?>
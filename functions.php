<?php
@session_start();

// Important functions
require_once('functions/front-nine-functions.php');
require_once('functions/community-functions.php');
require_once('functions/destination-functions.php');
require_once('functions/location-select-functions.php');
require_once('functions/other-functions.php');


//thumbnails
add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_theme_support' ) ) {
add_image_size( 'page-thumb', 459, 303, false );
add_image_size( 'scorecard-com-thumb', 180, 115, true );
add_image_size( 'scorecard-agent-thumb', 110, 124, false );


}

//add 'home' automatically to menu

function my_page_menu_args($args) {
	$args['show_home'] = true;
	return $args;
}
add_filter('wp_page_menu_args', 'my_page_menu_args');

//register menus
//register menus

function register_my_menus() {

register_nav_menus(

array(

'main-menu' => __( 'Top Navigation' ),
'footer-menu' => __( 'Footer Navigation' ),

)


);

}

add_action( 'init', 'register_my_menus' );


//Welcome Panel
if ( function_exists('register_sidebar') )

	register_sidebar(array(

	'name' => 'Welcome Panel',

	'before_widget' => '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">',

	'after_widget' => '</div>',

	'before_title' => '<h2 class="heading main-heading">',

	'after_title' => '</h2>',

)); 


//HP Tagline
if ( function_exists('register_sidebar') )

	register_sidebar(array(

	'name' => 'HP Tagline',

	'before_widget' => '',

	'after_widget' => '',

	'before_title' => '<h2>',

	'after_title' => '</h2>',

)); 


//Contact #
if ( function_exists('register_sidebar') )

	register_sidebar(array(

	'name' => 'Contact Number',

	'before_widget' => '',

	'after_widget' => '',

	'before_title' => '',

	'after_title' => '',

));

//Contact Email
if ( function_exists('register_sidebar') )

	register_sidebar(array(

	'name' => 'Contact Email',

	'before_widget' => '',

	'after_widget' => '',

	'before_title' => '',

	'after_title' => '',

)); 

//FB Link
if ( function_exists('register_sidebar') )

	register_sidebar(array(

	'name' => 'FB Link',

	'before_widget' => '',

	'after_widget' => '',

	'before_title' => '',

	'after_title' => '',

)); 

//Twitter Link
if ( function_exists('register_sidebar') )

	register_sidebar(array(

	'name' => 'Twitter Link',

	'before_widget' => '',

	'after_widget' => '',

	'before_title' => '',

	'after_title' => '',

)); 

//Pinterest Link
if ( function_exists('register_sidebar') )

	register_sidebar(array(

	'name' => 'Pinterest Link',

	'before_widget' => '',

	'after_widget' => '',

	'before_title' => '',

	'after_title' => '',

)); 


//Copyright Info
if ( function_exists('register_sidebar') )

	register_sidebar(array(

	'name' => 'Copyright Info',

	'before_widget' => '',

	'after_widget' => '',

	'before_title' => '',

	'after_title' => '',

)); 



//excerpt
function get_excerpt_by_id($post_id){
$the_post = get_post($post_id); //Gets post ID
$the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
$excerpt_length = 100; //Sets excerpt length by word count
$the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
$words = explode(' ', $the_excerpt, $excerpt_length + 1);
if(count($words) > $excerpt_length) :
array_pop($words);
array_push($words, ' ');
$the_excerpt = implode(' ', $words);
endif;
$the_excerpt = '<p>' . $the_excerpt . '</p>';
return $the_excerpt;
}

add_filter('excerpt_more','__return_false');



//Random Password Generator
function rand_passwd( $length = 8, $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' ) {
    return substr( str_shuffle( $chars ), 0, $length );
}




// OPENS AN EPUB FILE AND SEARCHES FOR 'OEBPS/content.opf' 
function zipopener($file) { 
  $EMAIL = "YOUR EMAIL ADDRESS HERE"; 
  $URL   = "YOUR SELECTED WEB ADDRESS HERE"; 
  $zip = zip_open($file); 
  if ($zip) { 
    // while ($zip_entry = zip_read($zip)) { 
    //   if (zip_entry_name($zip_entry) == 'retrade.xml') { 
    //   		echo file_get_contents('zip://'.zip_entry_name($zip_entry));
    //     if (zip_entry_open($zip, $zip_entry)) { 
    //       $contents = zip_entry_read($zip_entry); 

    //       echo $contents;
                     
    //       zip_entry_close($zip_entry); 
    //     } 
    //   } 
    // } 

  	copy('zip://'.$file.'#retrade.xml','propertylistings/retrade.xml');
    $result = file_get_contents('zip://'.$file.'#retrade.xml'); 
    echo $result;
    zip_close();
  } 
} 




add_filter('uwpqsf_result_tempt', 'customize_output', '', 4);
function customize_output($results , $arg, $id, $getdata ){
     // The Query
            $apiclass = new uwpqsfprocess();

            $last_meta_query = $arg['meta_query'];
            unset($arg['meta_query']);

            $query = new WP_Query( $arg );

            unset($arg['s']);
            $arg['meta_query'] = $last_meta_query;
            //unset($arg['meta_query'][1]);

            $query2 = new WP_Query( $arg );



            $merged = array_merge( $query->posts, $query2->posts );

			$post_ids = array();
			foreach( $merged as $item ) {
			    $post_ids[] = $item->ID;
			}

			$unique = array_unique($post_ids);


            $arg['post__in'] = $unique;

            unset($arg['meta_query']);
            unset($arg['tax_query']);

            $query3 = new WP_Query( $arg );


			 //$query = $query3;



        ob_start();    $result = '';
            // The Loop
 		$ctr = 0;
        if ( $query3->have_posts() ) { 
            while ( $query3->have_posts() ) {
            	
                $query3->the_post();


                if(count($arg['post__in'])==0) {
                    ?>
                        <span style="display: none;" class="search-count"><?php echo $ctr; ?> matches</span>
                        <script>
                        jQuery(document).ready(function($) {
                            var count = $('.search-count').html();
                            $('.sr-headline .sub-head').html(count);
                        });
                        </script>
                        <p>No 
                            <?php 

                                if($arg['post_type'][0] == 'communities'):
                                    echo "Community ";
                                elseif($arg['post_type'][0] == 'destinations'):
                                    echo "Destination ";
                                elseif($arg['role'] == 'agents'):
                                    echo "Agents ";
                                endif;
                            ?>

                        was found.</p>
                    <?php die();
                }

                $ctr++;

     ?>

 					<!-- search item -->
                    <div class="search-item">
                    	 
                        <?php
                        	//print_r($arg);
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail('small');  
                            }else{
                        ?>
                         <img src="<?php bloginfo('template_directory');?>/img/sample-community-pic.jpg" class="img-responsive" />
                         <?php
                            }
                        ?>
                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        <div class="sr-desc">
                       		 <?php 
                       		 	if(get_field('communitydescription')) {
                                	$desc=get_field('communitydescription');
                                } elseif (get_field('destinationdescription')) {
                                	$desc=get_field('destinationdescription');
                                }
                                $position = stripos ($desc, "."); //find first dot position

                                if($position) { //if there's a dot in our soruce text do
                                    $offset = $position + 1; //prepare offset
                                    $position2 = stripos ($desc, ".", $offset); //find second dot using offset
                                    $first_two = substr($desc, 0, $position2); //put two first sentences under $first_two

                                    echo $first_two . '.'; //add a dot
                                }

                                else {  //if there are no dots
                                    //do nothing
                                }
                            ?> 
                        </div>
                    
                    </div>
                    <!-- end search item -->

     <?php
            }
     ?>
     	<span style="display: none;" class="search-count"><?php echo $ctr; ?> matches</span>
		<script>
		jQuery(document).ready(function($) {
			var count = $('.search-count').html();
			$('.sr-headline .sub-head').html(count);
		});
		</script>
     <?php
                        echo  $apiclass->ajax_pagination($arg['paged'],$query->max_num_pages, 4, $id, $getdata);
         } else {
     ?>
		<span style="display: none;" class="search-count"><?php echo $ctr; ?> matches</span>
		<script>
		jQuery(document).ready(function($) {
			var count = $('.search-count').html();
			$('.sr-headline .sub-head').html(count);
		});
		</script>
        <p>No 
            <?php 
                if($arg['post_type'][0] == 'communities'):
                    echo "Community ";
                elseif($arg['post_type'][0] == 'destinations'):
                    echo "Destination ";
                elseif($arg['role'] == 'agents'):
                    echo "Agents ";
                endif;
            ?>

            was found.</p>
    <?php
                }
                /* Restore original Post Data */
                wp_reset_query();
 
        $results = ob_get_clean();        
            return $results;
}




function share_this_items( $permalink, $title ) {
    ?>

    <!-- SHARE -->
    <div id="shareModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content boxtype">
                <div class="modal-header noborders">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title mypopuptitle" id="myModalLabel">Social Share</h4>
                </div>
                <div class="modal-body">
                    <div class="addthis_toolbox addthis_default_style addthis_32x32_style" addthis:url="<?php echo $permalink; ?>" addthis:title="Finest Golf Property - <?php echo $title; ?>">
                        <a class="addthis_button_facebook at300b" title="Facebook" href="#"><span class=" at300bs at15nc at15t_facebook"><span class="at_a11y">Share on facebook</span></span></a>
                        <a class="addthis_button_twitter at300b" title="Tweet" href="#"><span class=" at300bs at15nc at15t_twitter"><span class="at_a11y">Share on twitter</span></span></a>
                        <a class="addthis_button_email at300b" target="_blank" title="Email" href="#"><span class=" at300bs at15nc at15t_email"><span class="at_a11y">Share on email</span></span></a>
                        <a class="addthis_button_pinterest_share at300b" target="_blank" title="Pinterest" href="#"><span class=" at300bs at15nc at15t_pinterest_share"><span class="at_a11y">Share on pinterest_share</span></span></a>
                        <a class="addthis_button_compact at300m" href="#"><span class=" at300bs at15nc at15t_compact"><span class="at_a11y">More Sharing Services</span></span></a>
                        <a class="addthis_counter addthis_bubble_style" href="#" tabindex="1000" style="display: inline-block;"><a class="addthis_button_expanded" target="_blank" title="View more services" href="#">5</a>
                        <a class="atc_s addthis_button_compact"><span></span></a></a>
                        <div class="atclear"></div>
                    </div>
                </div>
            </div>
        </div>  
    </div>

    <?php
}


function comdest_property_meta_box() {

    $screens = array( 'communities', 'destinations' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'comdest_property_sectionid','Properties',
            'comdest_property_callback',
            $screen
        );
    }
}
add_action( 'add_meta_boxes', 'comdest_property_meta_box' );




function comdest_property_callback( $post ) {

    // Add an nonce field so we can check for it later.
    wp_nonce_field( 'comdest_property_meta_box', 'comdest_property_meta_box_nonce' );

    /*
     * Use get_post_meta() to retrieve an existing value
     * from the database and use the value for the form.
     */
    $property_value = get_post_meta( $post->ID, '_comdest_property_value_key', true );


    $property_value = $property_value ? unserialize($property_value):array();

    echo '<label for="sample_field">';
    _e( 'Add properties to this community or destination');
    echo '</label> ';

    //echo '<input type="text" id="sample_field" name="sample_field" value="' . esc_attr( $value ) . '" size="25" />';
    

    if(function_exists('property_search')) {

        list($listing, $price) = property_search('admin');
    ?>
        <table id="propertyTable" class="display">
            <thead>
                <tr>
                    <th>Add</th>
                    <th>Listing Key</th>
                    <th>Property Title</th>
                    <th>Property Price</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach($listing as $key => $value) { ?>
                    <tr>
                        <td><input type="checkbox" name="property_field[]" value="<?php echo $value->ListingKey; ?>" <?php echo in_array((string)$value->ListingKey,$property_value) ? 'checked':'' ?> /> </td>
                        <td><?php echo $value->ListingKey; ?></td>
                        <td><?php echo $value->ListingTitle; ?></td>
                        <td><?php echo '$'.number_format((string)$value->ListPrice,2); ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <script>
        jQuery(document).ready(function($){
            $('#propertyTable').dataTable({
                "sScrollY": "450px",
                "bPaginate": false,

            });
        });
        </script>
    <?php

    }
}


function comdest_property_save_meta_box_data( $post_id ) {

    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */

    // Check if our nonce is set.
    if ( ! isset( $_POST['comdest_property_meta_box_nonce'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['comdest_property_meta_box_nonce'], 'comdest_property_meta_box' ) ) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    } else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    /* OK, it's safe for us to save the data now. */
    
    // Make sure that it is set.
    if ( ! isset( $_POST['property_field'] ) ) {
        return;
    }

    // Sanitize user input.
    $my_data = $_POST['property_field'];

    $properties = serialize($my_data);

    // Update the meta field in the database.
    update_post_meta( $post_id, '_comdest_property_value_key', $properties );
}
add_action( 'save_post', 'comdest_property_save_meta_box_data' );

<?php
/*

Template Name: Destination Videos Template

*/

?>
<?php get_header(); ?>

<div id="page-content">
	<div class="row">
    	<!-- three columns -->
      <?php include (TEMPLATEPATH . '/includes/destinations-left-sidebar.php'); ?>
        
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">
         			
                    <div id="page-links">
                    <!-- links -->
                    <ul>
                    <li><a href="<?php echo get_permalink(69); ?>">Destination Details</a></li>
                    <li><a href="<?php echo get_permalink(167); ?>">Photos</a></li>
                    <li><a href="<?php echo get_permalink(169); ?>">Location Map</a></li>
                    <li><a href="<?php echo get_permalink(173); ?>" class="selected">Video</a></li>
                    </ul>
                    </div>
                    
                    <!-- end links -->
                    <!-- START MAIN -->
                    <?php
                        $args = array(
                            'showposts' => 1, 
                            'post_type' => 'destinations',                            
                            'orderby'          => 'post_date',
                            'order'            => 'DESC',
                            'post_status'      => 'publish'
                                );
                            $the_query= new WP_query( $args);
                            //echo "before if";
                           //print_r($the_query);
                            $pstc=1;
                            //echo count($posts);
                             if($the_query->have_posts()){
                                while ( $the_query->have_posts() ) {
                                    $the_query->the_post();
                                    //the_field('communitydescription');

                                    //gallery images use the_content();
                                    //the_content();
                         
                    ?>
                    
                     <div id="mid-col-main">
                        <div class="featured-image">

                            
                            <div class="text-content">
                            <h2 class="community-name"><?php the_title(); ?></h2>
                            <h3 class="community-address"><?php the_field('destinationlocation');?></h3>
                            </div>
                            
                            <?php
                                $vids=get_field('destinationvideos');
                                if(!empty($vids) && $vids!=''){
                                    if(stripos($vids, "&&&")!== false){
                                        $vidsx=explode("&&&",$vids);
                                        foreach($vidsx as $vid){
                                            echo trim($vid)."<br />";
                            ?>
                                       <br />
                                        <!--iframe width="100%" height="315" src="<?php echo trim($vid);?>" frameborder="0" allowfullscreen></iframe-->
                            <?php
                                        }//end foreach
                                    }else{
                                        echo trim($vids);
                                    }//end stripos if
                                }
                            ?>     
                            
                            </div>
                    </div>
                    <?php
                            }//end while
                        }   //end if
                        wp_reset_postdata();
                        wp_reset_query();
                    ?>
                    <!-- END MAIN -->
            </div>
        
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
        
        	<div id="sidebar-r">

				            
            	<div class="item-box">
                <a href="#">
                <img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon1.png" class="sidebar-r-ico" /> 
                </a>
                <div class="sidebar-r-text">
                 <a href="#">Search All Properties
                in this Golf Destination
                </a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon2.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#">View Agents Specialized in this Golf Destination</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
                
                
                <div class="item-box">
                
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-icon3.png" class="sidebar-r-ico" /></a>
                
                <div class="sidebar-r-text">
                <a href="#">Get Answers</a>
                </div>
                
                	<div class="clearthis"></div>
                </div>
            
            
            
            <!-- scorecard -->
            
            <div class="scorecard">
            	<div class="scorecard-content">
            		<a href="#">Save To My Scorecard</a>
                </div>
            </div>
            
            
            <!-- more links -->
            
            <div class="more-links">
            	<a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-print.png" alt="Print" class="ml-iconf" /></a>
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-mail.png" alt="Mail" class="ml-icon" /></a>
                <a href="#"><img src="<?php bloginfo('template_directory');?>/img/sidebar-r-share.png" alt="Share" class="ml-icon" /></a>
            </div>
        
        
       	</div>
         
         
         	<!-- experts -->
   
        <div id="experts-box">
        	<div class="title-box">
        	<h2 >Carefree Experts</h2>
        	</div>
        
        	<div class="item-expert">
            	
                <img src="<?php bloginfo('template_directory');?>/img/expert-1.jpg" class="expert-img" />
                <div class="expert-desc">
                	<h3>Minim veniam</h3>
                    123-456-789
                    Lorem Ipsum
                    <br />
                    <a href="#">View more details »</a>
                </div>
                
                <div class="clearthis"></div>
            
            </div>
            
            
            
            	<div class="item-expert">
            	
                <img src="<?php bloginfo('template_directory');?>/img/expert-2.jpg" class="expert-img" />
                <div class="expert-desc">
                	<h3>Suscip Lorits</h3>
                    123-456-789
                    Lorem Ipsum
                    <br />
                    <a href="#">View more details »</a>
                </div>
                
                <div class="clearthis"></div>
            
           		</div>
                
                
                <div class="item-expert">
            	
                <img src="<?php bloginfo('template_directory');?>/img/expert-3.jpg" class="expert-img" />
                <div class="expert-desc">
                	<h3>Lorebore Mith</h3>
                    123-456-789
                    Lorem Ipsum
                    <br />
                    <a href="#">View more details »</a>
                </div>
                
                <div class="clearthis"></div>
            
           		</div>
            
        </div>
        
        
        <!-- end experts -->
        
        </div>
        <!-- end three columns -->
        
     
        <div class="clearthis"></div>
        
        
        
		</div>
    </div>
</div>

<?php get_footer(); ?>
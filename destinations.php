<?php
/*

Template Name: Destination Search Template

*/

?>
<?php get_header(); ?>
                    
<div id="page-content">
	<div class="row">
            <?php destination_left_section(); ?>
            
        
        	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">
         			
                    <div id="page-links">
                        <?php get_template_part('includes/tab/tab','destination-links' ); ?>
                    </div>
                    
                    <!-- end links -->
                    
                    <!-- START MAIN -->
                    <?php

                    if(isset($_GET['dscountry']) && isset($_GET['dsstate']) && isset($_GET['dscity'])) {
                            $country_id = (int) mysql_real_escape_string(trim($_GET['dscountry']));
                            $state_id = mysql_real_escape_string(trim($_GET['dsstate']));
                            $city_id = (int) mysql_real_escape_string(trim($_GET['dscity']));

                            $args = array(
                                'showposts' => 1, 
                                'post_type' => 'destinations',
                                'meta_query' => array(
                                        array(
                                            'key' => 'destinationlocation2',
                                            'value' => ':"'.$country_id.'";',
                                            'compare' => 'LIKE'
                                        ),
                                        array(
                                            'key' => 'destinationlocation2',
                                            'value' => ':"'.$city_id.'";',
                                            'compare' => 'LIKE'
                                        ),
                                        array(
                                            'key' => 'destinationlocation2',
                                            'value' => ':"'.$state_id.'";',
                                            'compare' => 'LIKE'
                                        ),
                                    ),                               
                                'orderby'          => 'post_date',
                                'order'            => 'DESC',
                                'post_status'      => 'publish'
                                    );
                    } else {
                            $args = array(
                                'showposts' => 1, 
                                'post_type' => 'destinations',                            
                                'orderby'          => 'post_date',
                                'order'            => 'DESC',
                                'post_status'      => 'publish'
                                    );
                    }
                            $the_query= new WP_query( $args);
                            //echo "before if";
                           //print_r($the_query);
                            $pstc=1;
                            //echo count($posts);
                             if($the_query->have_posts()){
                                while ( $the_query->have_posts() ) {
                                    $the_query->the_post();
                                    //the_field('communitydescription');

                                    //gallery images use the_content();
                                    //the_content();
                         
                    ?>
                    <div id="mid-col-main">
                        <div class="featured-image">
                            <div class="tab-content">
                                <div class="tab-pane active" id="community">
                                     <?php get_template_part('includes/tab/destination','details-tab-content' );?>
                                </div> 
                                <!-- end community tab -->

                                <div class="tab-pane" id="photos">
                                    <?php get_template_part('includes/tab/destination','photos-tab-content' ); ?>
                                </div>
                                <!-- end photos tab -->

                                <div class="tab-pane" id="location">
                                    <?php get_template_part('includes/tab/destination','location-tab-content' ); ?>
                                </div>
                                <!--end location tab -->

                                <div class="tab-pane" id="video">
                                    <?php get_template_part('includes/tab/destination','video-tab-content' ); ?>
                                </div>
                                <!-- end vides tab -->
                            </div> <!-- end tab-content -->                        
                        </div><!-- END featured image -->
                    </div><!-- END MAIN -->

                    <?php
                                }//end while
                            } else {
                                echo "No Destinations found.";
                            }   //end if
                            wp_reset_postdata();
                            wp_reset_query();
                        ?>
                    <!-- END MAIN -->
            </div>
        
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
        
        	<?php get_sidebar('destination2'); ?>
        
        </div>
        <!-- end three columns -->
        
     
        <div class="clearthis"></div>
        
        <?php destination_modal(); ?>
        
        
		</div>
    </div>
</div>

<?php get_footer(); ?>
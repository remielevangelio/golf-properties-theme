<?php
/*

Template Name: Community Template

*/

?>
<?php get_header(); ?>

<div id="page-content">
    <div class="row">
        <!-- three columns -->
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 left-col">
         
            <div class="search-results">
            
                <?php community_left_section(); ?>
                
            </div>
        </div>
        
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mid-col">

                    <div id="page-links">
                        <?php get_template_part('includes/tab/tab','links' );?>
                    </div>
                    
                    <!-- end links -->
                    <?php
                    $args = get_community_arg(1); //function is in functions/community-functions.php


                     $the_query= new WP_query( $args);
                            $pstc=1;
                            $ctr = 0;
                            if($the_query->have_posts())
                            {
                                while ( $the_query->have_posts() ) 
                                {
                                    // $ctr++;
                                    $the_query->the_post();
                                    $the_location = get_post_meta(get_the_ID(), 'location_2');

                                    // if($ctr == 1) {
                                        community_main_content();
                                    // }

                                }
                            } 
                            else 
                            {
                                echo "No Communities found.";

                            }


                           
                    ?>
            </div>
        
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 right-col">
            <?php get_sidebar('community2'); ?>
        </div>
        <!-- end three columns -->
        
     
        <div class="clearthis"></div>
        
    
        
        
        </div>
    </div>
</div>

<?php community_modal(); ?>

<?php get_footer(); ?>